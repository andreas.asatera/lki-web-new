<?php

namespace App\Http\Controllers\Admin;

use App\News;
use Arr;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Te7aHoudini\LaravelTrix\Models\TrixAttachment;
use Te7aHoudini\LaravelTrix\Models\TrixRichText;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $news = News::all();
        return view('admin.news.index')->with([
            'newscollection' => $news
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $news = new News();
        $news->judul = $request->judul;
        if ($request->has('image')) $news->image = $request->image->store('news');
        $saved = $news->save();
        $req_richtext = request('news-trixFields');
        $req_attachment = request('attachment-news-trixFields');
        foreach ($req_richtext as $field => $content) {
            TrixRichText::updateOrCreate([
                'model_id' => $news->id,
                'model_type' => $news->getMorphClass(),
                'field' => $field,
            ], [
                'field' => $field,
                'content' => $content,
            ]);
            $attachments = Arr::get($req_attachment, $field, []);
            TrixAttachment::whereIn('attachment', is_string($attachments) ? json_decode($attachments) : $attachments)
                ->update([
                    'is_pending' => 0,
                    'attachable_id' => $news->id,
                ]);
        }
        if ($saved) return redirect()->route('news.index')->with('success', 'Sukses menambah berita: <b>'. $request->judul .'</b>');
        else return redirect()->back()->with('error', 'Gagal menambah berita: <b>'. $request->judul .'</b>');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $news = News::find($id);
        return view('admin.news.edit')->with([
            'news' => $news
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $news = News::find($id);
        $news->judul = $request->judul;
        if ($request->has('image')) {
            File::delete(public_path("storage/$news->image"));
            $news->image = $request->image->store('news');
        }
        $saved = $news->save();
        $req_richtext = request('news-trixFields');
        $req_attachment = request('attachment-news-trixFields');
        foreach ($req_richtext as $field => $content) {
            TrixRichText::updateOrCreate([
                'model_id' => $news->id,
                'model_type' => $news->getMorphClass(),
                'field' => $field,
            ], [
                'field' => $field,
                'content' => $content,
            ]);
            $attachments = Arr::get($req_attachment, $field, []);
            TrixAttachment::whereIn('attachment', is_string($attachments) ? json_decode($attachments) : $attachments)
                ->update([
                    'is_pending' => 0,
                    'attachable_id' => $news->id,
                ]);
        }
        if ($saved) return redirect()->back()->with('success', 'Sukses menambah berita: <b>'. $request->judul .'</b>');
        else return redirect()->back()->with('error', 'Gagal menambah berita: <b>'. $request->judul .'</b>');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $news = News::find($id);

        // Rich Text
        $trixRichText = $news->trixRichText()->where('field','isi')->first();
        $trixRichText->delete();

        // Rich Attachment
        $trixAttachments = $news->trixAttachments()->where('field','isi')->first();
        if (isset($trixAttachments)){
            File::delete(public_path("storage/attachment/$trixAttachments->attachment"));
            $trixAttachments->delete();
        }

        // Delete News Image
        if ($news->image != null) File::delete(public_path("storage/$news->image"));

        // Delete News
        $deleted = $news->delete();

        if ($deleted) return redirect()->back()->with('success', 'Berhasil menghapus berita');
        else return redirect()->back()->with('error', 'Gagal menghapus berita');
    }
}
