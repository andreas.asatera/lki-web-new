<?php

namespace App\Http\Controllers\Admin;

use App\Payment;
use App\Subscription;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $payment = Payment::where('active', 2)->orWhere('active', 3)->with('user')->get();
        return response(view('admin.payment.index')->with([
            'payments' => $payment
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if ($request->has('end_date')) {
            $end_date = Carbon::createFromFormat(config('app.date_format'), $request->end_date);
            if ($request->has('start_date')) {
                if (!empty($request->start_date)){
                    $start_date = Carbon::createFromFormat(config('app.date_format'), $request->start_date);
                    if (strtotime($start_date) > strtotime($end_date)){
                        return redirect()->back()->with([
                            'error' => 'Gagal merubah subscription: <b>tanggal akhir langganan tidak boleh lebih kecil dari tangal awal langganan</b>',
                            'users' => User::with('subscription')->get()
                        ]);
                    }
                }
                if (strtotime(Carbon::now()) > strtotime($end_date)){
                    return redirect()->back()->with([
                        'error' => 'Gagal merubah subscription: <b>tanggal akhir langganan tidak boleh lebih kecil dari tangal sekarang</b>',
                        'users' => User::with('subscription')->get()
                    ]);
                }
            }
            $subscribe = Subscription::updateOrCreate(
                ['uid' => $id],
                [
                    'start_date' => Carbon::now()->format(config('app.date_format')),
                    'end_date' => $request->end_date,
                    'type_subs' => $request->type_subs
                ]
            );
            if ($subscribe->wasRecentlyCreated) {
                $payment = Payment::find($request->payment_id);
                $payment->active = 3;
                $payment->save();
                return redirect()->back()->with([
                        'success' => 'Berhasil menambah subscription: <b> ' . $subscribe->user->name . '</b>',
                        'users' => User::with('subscription')->get()
                    ]
                );
            }
            if ($subscribe->wasChanged()) {
                $payment = Payment::find($request->payment_id);
                $payment->active = 3;
                $payment->save();
                return redirect()->back()->with([
                    'success' => 'Berhasil mengubah subscription: <b> ' . $subscribe->user->name . '</b>',
                    'users' => User::with('subscription')->get()
                ]);
            }
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
