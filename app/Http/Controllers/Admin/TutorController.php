<?php

namespace App\Http\Controllers\Admin;

use App\Tutor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TutorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tutor = Tutor::all();
        return view("admin.tutor.index")->with([
            'tutors' => $tutor
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view("admin.tutor.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $tutor = new Tutor();
        $tutor->nama = $request->nama;
        $tutor->nohp = $request->nohp;
        $saved = $tutor->save();
        if ($saved){
            return redirect()->back()->with('success', 'Tutor: <b>' . $request->nama .'</b> berhasil ditambahkan');
        }
        else {
            return redirect()->back()->with('error', 'Gagal menambahkan tutor');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $tutor = Tutor::find($id);
        $tutor->nama = $request->nama;
        $tutor->nohp = $request->nohp;
        $saved = $tutor->save();
        if ($saved){
            return redirect()->back()->with('success', 'Tutor: <b>' . $request->nama .'</b> berhasil diubah');
        }
        else {
            return redirect()->back()->with('error', 'Gagal menambahkan tutor');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
