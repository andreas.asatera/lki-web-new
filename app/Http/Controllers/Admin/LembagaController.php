<?php

namespace App\Http\Controllers\Admin;

use App\Lembaga;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LembagaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $lembaga_arr = Lembaga::all();
        return response(view('admin.lembaga.index')->with([
            'lembaga_arr' => $lembaga_arr
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $numbers = str_split((string)(int)microtime(true));
        shuffle($numbers);
        $rand = '';
        foreach (array_rand($numbers, 6) as $k) $rand .= $numbers[$k];

        $lembaga = new Lembaga();
        $lembaga->nama = $request->nama;
        $lembaga->kode = $rand;
        $saved = $lembaga->save();
        if ($saved) return redirect()->back()->with('success', "Penambahan lembaga berhasil");
        else return redirect()->back()->with('error', "Penambahan lembaga gagal");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $lembaga = Lembaga::find($id);
        $lembaga->nama = $request->nama;
        $saved = $lembaga->save();
        if ($saved) return redirect()->back()->with('success', "Berhasil mengubah data lembaga");
        else return redirect()->back()->with('error', "Gagal mengubah data lembaga");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $users = User::where('lembaga', $id)->get();
        foreach ($users as $user) {
            $user->lembaga = 'default';
            $user->save();
        }
        $lembaga = Lembaga::find($id);
        $lembaga->delete();
        return redirect()->back()->with('success', "Berhasil menghapus data lembaga");
    }
}
