<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\ImportJob;
use App\Jobs\ImportSubscription;
use App\Subscription;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response(view('admin.subscription.index')->with([
            'users' => User::with('subscription')->get()
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return response(view('admin.subscription.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if ($request->has('end_date')) {
            $end_date = Carbon::createFromFormat(config('app.date_format'), $request->end_date);
            if ($request->has('start_date')) {
                if (!empty($request->start_date)){
                    $start_date = Carbon::createFromFormat(config('app.date_format'), $request->start_date);
                    if (strtotime($start_date) > strtotime($end_date)){
                        return redirect()->back()->with([
                            'error' => 'Gagal merubah subscription: <b>tanggal akhir langganan tidak boleh lebih kecil dari tangal awal langganan</b>',
                            'users' => User::with('subscription')->get()
                        ]);
                    }
                }
                if (strtotime(Carbon::now()) > strtotime($end_date)){
                    return redirect()->back()->with([
                        'error' => 'Gagal merubah subscription: <b>tanggal akhir langganan tidak boleh lebih kecil dari tangal sekarang</b>',
                        'users' => User::with('subscription')->get()
                    ]);
                }
            }
            $subscribe = Subscription::updateOrCreate(
                ['uid' => $id],
                [
                    'start_date' => Carbon::now()->format(config('app.date_format')),
                    'end_date' => $request->end_date,
                    'type_subs' => $request->type_subs
                ]
            );
            if ($subscribe->wasRecentlyCreated) {
                return redirect()->back()->with([
                        'success' => 'Berhasil menambah subscription: <b> ' . $subscribe->user->name . '</b>',
                        'users' => User::with('subscription')->get()
                    ]
                );
            }
            if ($subscribe->wasChanged()) {
                return redirect()->back()->with([
                    'success' => 'Berhasil mengubah subscription: <b> ' . $subscribe->user->name . '</b>',
                    'users' => User::with('subscription')->get()
                ]);
            }
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $subscribe = Subscription::where('uid', $id)->with('user')->first();
        $subscribe->start_date = null;
        $subscribe->end_date = null;
        $subscribe->type_subs = null;
        $saved = $subscribe->save();
        if ($saved) return redirect()->back()->with([
            'success' => 'Berhasil menghapus langganan: <b>' . $subscribe->user->name . '</b>',
            'users' => User::with('subscription')->get()
        ]);
        elseif (!$saved) return redirect()->back()->with([
            'error' => 'Gagal menghapus langganan: <b>' . $subscribe->user->name . '</b>',
            'users' => User::with('subscription')->get()
        ]);
    }

    public function bulk(Request $request)
    {
        //
        try {
            $this->validate($request, [
                'file' => 'required'
            ]);
        } catch (ValidationException $e) {
        }

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();

            $file->storeAs(
                'import', $filename
            );
            $this->replaceDelimiters(public_path('storage/import/' . $filename));
            ImportSubscription::dispatch($filename, $request->start_date, $request->end_date, $request->type_subs);

            return redirect()->back()->with('success', 'Upload success');
        }
        return redirect()->back()->with('error', 'Failed to upload file');
    }

    private function replaceDelimiters($file)
    {
        // Delimiters to be replaced: pipe, comma, semicolon, caret, tabs
        $delimiters = array('|', ';', '^', "\t");
        $delimiter = ',';

        $str = file_get_contents($file);
        $str = str_replace($delimiters, $delimiter, $str);
        file_put_contents($file, $str);
    }
}
