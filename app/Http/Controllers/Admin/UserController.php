<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\ImportJob;
use App\Lembaga;
use App\Ortu;
use App\Sekolah;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response(view('admin.user.index')->with([
            'users' => User::with('rel_lembaga')->get()
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function create()
    {
        //
        /*$sekolah = Sekolah::all();
        if (!$sekolah->count()){
            return redirect()->route('sekolah.create')->with('warning', 'Tambah sekolah terlebih dahulu sebelum menambah user');
        }
        return response(view('admin.user.create')->with([
            'sekolahs' => $sekolah
        ]));*/
        return response(view('admin.user.create')->with([
            'lembaga_arr' => Lembaga::all()
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function single(Request $request)
    {
        //
        $user = new User();
        $user->lembaga = $request->lembaga;
        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->has('gender')) $user->gender = $request->gender;
        if ($request->has('tgl_lahir')) $user->tgl_lahir = Carbon::parse($request->tgl_lahir)->format('Y/m/d');
        if ($request->has('alamat')) $user->alamat = $request->alamat;
        $user->no_hp = $request->no_hp;
        $user->password = bcrypt($request->password);
        $user->save();
        return redirect()->back()->with('success', 'Sukses menambah user: <b>'. $user->name .'</b>');
    }

    public function bulk(Request $request)
    {
        //
        try {
            $this->validate($request, [
                'file' => 'required'
            ]);
        } catch (ValidationException $e) {
        }

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();

            $file->storeAs(
                'import', $filename
            );

            $this->replaceDelimiters(public_path('storage/import/' . $filename));
            ImportJob::dispatch($filename, $request->lembaga, $request->date_end);

            return redirect()->back()->with('success', 'Upload success');
        }
        return redirect()->back()->with('error', 'Failed to upload file');
    }

    private function replaceDelimiters($file)
    {
        // Delimiters to be replaced: pipe, comma, semicolon, caret, tabs
        $delimiters = array('|', ';', '^', "\t");
        $delimiter = ',';

        $str = file_get_contents($file);
        $str = str_replace($delimiters, $delimiter, $str);
        file_put_contents($file, $str);
    }

    public function resetpass($id)
    {
      $user = User::find($id);
      $user->password = bcrypt(123456);
      $user->save();
      return redirect()->back()->withSuccess('Berhasil mereset password <b>'. $user->name .'</b> menjadi <b>123456</b>');
    }
}
