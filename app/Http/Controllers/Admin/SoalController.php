<?php

namespace App\Http\Controllers\Admin;

use App\Kategori;
use App\Soal;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function index()
    {
        //
        $soal = Soal::all();
        if (!Kategori::all()->count()){
            return redirect()->route('kategori.create')->with('error', 'Buat kategori terlebih dahulu');
        }
        return response(view('admin.soal.index')->with([
            'soals' => $soal
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function create()
    {
        //
        $kategori = Kategori::all();
        if (!$kategori->count()){
            return redirect()->route('kategori.create')->with('error', 'Buat kategori terlebih dahulu');
        }
        return response(view('admin.soal.create')->with([
            'kategories' => $kategori
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $soal = new Soal();
        $soal->question = $request->question;
        if ($request->hasFile('image')){
            $soal->image = $request->image->store('soal');
        }
        $soal->option1 = $request->option1;
        $soal->option2 = $request->option2;
        $soal->option3 = $request->option3;
        $soal->option4 = $request->option4;
        $soal->answerNr = $request->answerNr;
        $soal->categoryID = $request->categoryID;
        $soal->save();
        return redirect()->back()->with('success', 'Berhasil menambahkan soal #'. $soal->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $kategori = Kategori::all();
        if (!$kategori->count()){
            return redirect()->route('kategori.create')->with('error', 'Buat kategori terlebih dahulu');
        }
        $soal = Soal::find($id);
        return response(view('admin.soal.edit')->with([
            'kategories' => $kategori,
            'soal' => $soal
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $soal = Soal::find($id);
        $soal->question = $request->question;
        if ($request->hasFile('image')){
            File::delete(public_path('storage/'.$soal->image));
            $soal->image = $request->image->store('soal');
        }
        $soal->option1 = $request->option1;
        $soal->option2 = $request->option2;
        $soal->option3 = $request->option3;
        $soal->option4 = $request->option4;
        $soal->answerNr = $request->answerNr;
        $soal->categoryID = $request->categoryID;
        $soal->save();
        return redirect()->back()->with('success', 'Berhasil menambahkan soal #'. $soal->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
