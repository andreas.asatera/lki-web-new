<?php

namespace App\Http\Controllers\admin;

use App\Kategori;
use App\Lembaga;
use App\Score;
use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $lembaga_arr = Lembaga::all();
        $materi_arr = Kategori::all();
        return response(view('admin.score.index')->with([
            'lembaga_arr' => $lembaga_arr,
            'materi_arr' => $materi_arr
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $users = User::where('lembaga', $request->lembaga)->with(['score' => function ($q) use ($request) {
            $q->where('cid', $request->cid);
        }])->get();
        return response(view('admin.score.show')->with([
            'users' => $users,
            'lembaga' => Lembaga::where('kode', $request->lembaga)->first(),
            'kategori' => Kategori::where('id', $request->cid)->first()
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deleteScoreCID(Request $request)
    {
        $users = User::where('lembaga', $request->lembaga)->pluck('id');
        $score = Score::where('cid', $request->cid)->whereIn('uid', $users)->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus laporan nilai');
    }

    public function massScoreDelete(Request $request)
    {
        $users = User::where('lembaga', $request->lembaga)->pluck('id');
        $score = Score::whereIn('uid', $users)->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus laporan nilai');
    }

    public function massScoreDeleteView()
    {
        return view('admin.score.delete')->with([
            'lembaga_arr' => Lembaga::all()
        ]);
    }
}
