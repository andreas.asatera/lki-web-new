<?php

namespace App\Http\Controllers\Admin;

use App\Kategori;
use App\Materi;
use File;
use FFMpeg\FFMpeg;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MateriController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
  */
  public function index()
  {
    //
    $materi = Materi::with('category')->get();
    $kategori = Kategori::all();
    if (!$kategori->count()){
      return redirect()->route('kategori.create')->with('error', 'Buat kategori terlebih dahulu');
    }
    return response(view('admin.materi.index')->with([
      'materies' => $materi,
      'kategories' => $kategori
    ]));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
  */
  public function create()
  {
    //
    $kategori = Kategori::all();
    if (!$kategori->count()){
      return redirect()->route('kategori.create')->with('error', 'Buat kategori terlebih dahulu');
    }
    return response(view('admin.materi.create')->with([
      'kategories' => $kategori
    ]));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //
    //        $destinationPath = 'uploads';
    $materi = new Materi();

    $materi->judul = $request->judul;

    $materi->ringkasan = $request->ringkasan;

    $materi->materi = $request->materi->store('materi');

    $materi->video = $request->video->store('video');

    $materi->kategori = $request->kategori;

    $materi->save();

    // Thumbnail
    if ($request->hasFile('video')) {
      $video = realpath(public_path('storage/'. $materi->video));
      $thumbnail_location = realpath(public_path('storage/thumbnail/'));
      $title = explode('.', explode('/', $materi->video)[1])[0].".jpg";
      exec("ffmpeg -i '$video' -ss 00:00:01 -vframes 1 -vf scale=720:480 '$thumbnail_location/$title'");
      $materi->thumbnail = 'thumbnail/'.$title;
      $materi->save();
    }

    return redirect()->back()->with('success', 'Berhasil menambahkan materi: <b> '. $request->judul .'</b>');
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
    $kategori = Kategori::all();
    if (!$kategori->count()){
      return redirect()->route('kategori.create')->with('error', 'Buat kategori terlebih dahulu');
    }
    $materi = Materi::find($id);
    return response(view('admin.materi.edit')->with([
      'kategories' => $kategori,
      'materi' => $materi
    ]));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
    $materi = Materi::find($id);

    $materi->judul = $request->judul;

    $materi->ringkasan = $request->ringkasan;

    if ($request->hasFile('materi')){
      File::delete(public_path('storage/'.$materi->materi));
      $materi->materi = $request->materi->store('materi');
    }


    if ($request->hasFile('video')){
      File::delete(public_path('storage/'.$materi->video));
      $materi->video = $request->video->store('video');
    }

    $materi->kategori = $request->kategori;

    $materi->save();

    // Thumbnail
    if ($request->hasFile('video')) {
      File::delete(public_path('storage/'.$materi->thumbnail));
      $video = realpath(public_path('storage/'. $materi->video));
      $thumbnail_location = realpath(public_path('storage/thumbnail/'));
      $title = explode('.', explode('/', $materi->video)[1])[0].".jpg";
      exec("ffmpeg -i '$video' -ss 00:00:01 -vframes 1 -vf scale=720:480 '$thumbnail_location/$title'");
      $materi->thumbnail = 'thumbnail/'.$title;
      $materi->save();
    }

    return redirect()->back()->with('success', 'Berhasil mengubah materi: <b> '. $request->judul .'</b>');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }

  public function get($type, $name, $ext){
    $path = public_path("storage/$type/$name.$ext");
    if ($ext == 'pdf') $extension = 'application/pdf';
    if ($ext == 'mp4') $extension = 'video/mp4';

    return response()->file($path, [
      'Content-Type' => $extension,
      'Content-Disposition' => 'inline; filename="'. $name.$ext .'"'
    ]);
  }

  public function generateThumbs(){
    $materi = Materi::all();
    foreach ($materi as $key) {
      File::delete(public_path('storage/'.$key->thumbnail));
      $video = realpath(public_path('storage/'. $key->video));
      $thumbnail_location = realpath(public_path('storage/thumbnail/'));
      $title = explode('.', explode('/', $key->video)[1])[0].".jpg";
      exec("ffmpeg -i '$video' -ss 00:00:01 -vframes 1 -vf scale=720:480 '$thumbnail_location/$title'");
      $key->thumbnail = 'thumbnail/'.$title;
      $key->save();
    }
    return redirect()->back();
  }
}
