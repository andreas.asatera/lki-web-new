<?php

namespace App\Http\Controllers\API;

use App\Kategori;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function index()
    {
        //
        switch (Auth::user()->subscription->type_subs){
            case 1: // trial
                $category = Kategori::where('type_subs', 1)->with('materi')->withCount(['score' => function($q){
                    $q->where('uid', Auth::id());
                }])->with(['score' => function ($q) {
                    $q->where('score', '>=', 60);
                    $q->where('uid', Auth::id());
                }])->get();
                break;

            case 2: // beginner
                $category = Kategori::where('type_subs', '<=', 2)->with('materi')->withCount(['score' => function($q){
                    $q->where('uid', Auth::id());
                }])->with(['score' => function ($q) {
                    $q->where('score', '>=', 60);
                    $q->where('uid', Auth::id());
                }])->get();
                break;

            case 3: // advance
                $category = Kategori::where('type_subs', '<=', 3)->with('materi')->withCount(['score' => function($q){
                    $q->where('uid', Auth::id());
                }])->with(['score' => function ($q) {
                    $q->where('score', '>=', 60);
                    $q->where('uid', Auth::id());
                }])->get();
                break;

            case 4: // advance
                $category = Kategori::with('materi')->withCount(['score' => function($q){
                    $q->where('uid', Auth::id());
                }])->with(['score' => function ($q) {
                    $q->where('score', '>=', 60);
                    $q->where('uid', Auth::id());
                }])->get();
                break;
        }
        if ($category->count() == 0) {
            return response()->json([
                'status' => 0,
            ]);
        } else {
            return response()->json([
                'status' => 1,
                'category' => $category
            ]);
        }
    }

    public function index_materi()
    {
        $category = Kategori::has('materi')->with('materi')->get();
        if ($category->count()) {
            return response()->json([
                'status' => 1,
                'category' => $category
            ]);
        } else {
            return response()->json([
                'status' => 2,
                'message' => 'Belum tersedia kategori materi'
            ]);
        }
    }

    public function index_soal()
    {
        $category = Kategori::has('soal')->get();
        if ($category->count()) {
            return response()->json([
                'status' => 1,
                'category' => $category
            ]);
        } else {
            return response()->json([
                'status' => 2,
                'message' => 'Belum tersedia kategori soal'
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
