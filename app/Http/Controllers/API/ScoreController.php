<?php

namespace App\Http\Controllers\API;

use App\Check;
use App\Kategori;
use App\Score;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function index()
    {
        //
//        $score = Score::where('uid', \Auth::id())->with('kategori')->get();
        $kategori = Kategori::with(['score_user' => function ($q) {
            $q->where('uid', Auth::id());
            $q->where('score', '>=', 60);
        }])->withCount(['score' => function($q){
            $q->where('uid', Auth::id());
        }])->get();
        if ($kategori->count()) {
            return response()->json([
                'status' => 1,
                'totalCat' => $kategori->count(),
                'kategori' => $kategori
            ]);
        } else {
            return response()->json([
                'status' => 0,
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $score = new Score();
        $score->uid = Auth::id();
        $score->cid = $request->cid;
        $score->score = $request->score;
        $saved = $score->save();
        if (!$saved) {
            return response()->json([
                'status' => 0
            ]);
        } else {
            return response()->json([
                'status' => 1
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $score = Score::where([
            'uid' => Auth::id(),
            'cid' => $id
        ])->with('kategori')->get();
        return response()->json([
            'score' => $score
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function check(Request $request)
    {
        $check = Check::where([
            'uid' => Auth::id(),
            'cid' => $request->cid
        ])->first();
        if (!isset($check)) {
            return response()->json([
                'status' => 0
            ]);
        } else {
            $score = Score::where([
                ['uid', Auth::id()],
                ['cid', $request->cid]
            ])->get();
            $data['status'] = 1;
            if (!isset($score)) {
                $data['row'] = 0;
            } else {
                if ($score->count() < 4) {
                    $data['row'] = 0;
                    $data['score_count'] = $score->count();
                }
                else $data['row'] = 1;
            }
            return $data;
        }
    }
}
