<?php

namespace App\Http\Controllers\API;

use App\Payment;
use App\Subscription;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    public $inc = 0;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $payment = Payment::where([
            ['user_id', Auth::id()],
            ['type_subs', '=', $request->type_subs]
        ])->get();
        if (!count($payment)) {
            $number = $this->generateUniqueNumber();
            if ($number != "error") {
                $payment_new = new Payment();
                $payment_new->user_id = Auth::id();
                $payment_new->expire = Carbon::now()->addDays(1)->format('Y-m-d H:i:s');
                $payment_new->active = 1;
                $payment_new->unique_number = $number;
                $payment_new->total = $request->harga + $number;
                $payment_new->type_subs = $request->type_subs;
                $payment_new->save();
                $diff = ['diff' => Carbon::now()->diffInSeconds(Carbon::parse($payment_new->expire), false)];
                $pay_array = array_merge($payment_new->toArray(), $diff);
                return response()->json([
                    'status' => 1,
                    'payment' => $pay_array
                ]);
            } else return response()->json([
                'status' => 0,
                'message' => "Error happened while creating unique number. Please try again later"
            ]);
        } else {
            if ($payment->contains('active', 1)) {
                $payment_first = $payment->where('active', 1)->first();
                if ($payment_first->active == 1) {
                    if (!isset($payment->image)) {
                        if (Carbon::now() <= Carbon::parse($payment_first->expire)) {
                            $diff = ['diff' => Carbon::now()->diffInSeconds(Carbon::parse($payment_first->expire), false)];
                            $pay_array = array_merge($payment_first->toArray(), $diff);
                            return response()->json([
                                'status' => 1,
                                'payment' => $pay_array,
                            ]);
                        } else {
                            $payment_first->active = 0;
                            $payment_first->save();
                            return response()->json([
                                'status' => 3,
                                'message' => 'Payment expired. Please generate a new one.'
                            ]);
                        }
                    } else {
                        return response()->json([
                            'status' => 4,
                            'message' => 'Payment still in verification by admin.'
                        ]);
                    }
                }
            }
            elseif ($payment->contains('active', 2)) {
                return response()->json([
                    'status' => 4,
                    'message' => 'Payment still in verification by admin.'
                ]);
            }
            else {
                $number = $this->generateUniqueNumber();
                if ($number != "error") {
                    $payment_new = new Payment();
                    $payment_new->expire = Carbon::now()->addDays(1)->format('Y-m-d H:i:s');
                    $payment_new->active = 1;
                    $payment_new->user_id = Auth::id();
                    $payment_new->unique_number = $number;
                    $payment_new->total = $request->harga + $number;
                    $payment_new->type_subs = $request->type_subs;
                    $payment_new->save();
                    $diff = ['diff' => Carbon::now()->diffInSeconds(Carbon::parse($payment_new->expire), false)];
                    $pay_array = array_merge($payment_new->toArray(), $diff);
                    return response()->json([
                        'status' => 1,
                        'payment' => $pay_array
                    ]);
                } else return response()->json([
                    'status' => 0,
                    'message' => "Error happened while creating unique number. Please try again later"
                ]);
            }
        }
    }

    function generateUniqueNumber()
    {
        if ($this->inc <= 10) {
            $this->inc++;
            $number = rand(1, 2000);
            if ($this->numberExists($number)) {
                return $this->generateUniqueNumber();
            }
            return $number;
        } else {
            $number = "error";
            return $number;
        }
    }

    function numberExists($number)
    {
        return Payment::where('unique_number', $number)->exists();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function upload(Request $request)
    {
        $payment = Payment::where([
            "user_id" => Auth::id(),
            "unique_number" => $request->unique_number,
            "type_subs" => $request->type_subs,
            "active" => 1
        ]);
        $subscription = Subscription::where('uid', Auth::id())->first();
        if (isset($subscription)){
            $subscription->delete();
        }
        if ($payment->exists()) {
            $first = $payment->first();
            $first->image = $request->pic->store('payment');
            $first->active = 2;
            $saved = $first->save();
            if ($saved) {
                return response()->json([
                    'status' => 1
                ]);
            } else {
                return response()->json([
                    'status' => 0,
                ]);
            }
        } else {
            return response()->json([
                'status' => 0,
            ]);
        }
    }
}
