<?php

namespace App\Http\Controllers\API;

use App\Payment;
use App\Subscription;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function index()
    {
        //
        $payment_status = 0;
        $subscribe = Subscription::where('uid', Auth::id())->with('payment')->latest()->first();
        if (!isset($subscribe)) {
            $payment = Payment::where('user_id', Auth::id())->get();
            if (isset($payment)){
                if ($payment->contains('active', 1)){
                    $payment_status = 1;
                }
                elseif ($payment->contains('active', 2)){
                    $payment_status = 2;
                }
                return response()->json([
                    'status' => 0,
                    'payment' => $payment_status,
                ]);
            }
        }
        else {
            if (isset($subscribe->end_date)){
                $daysleft = Carbon::parse(Carbon::now())->diffInDays(Carbon::createFromFormat(config('app.date_format'), Auth::user()->subscription->end_date), false);
                if ($daysleft >= 0){
                    $subscribe = array_merge($subscribe->toArray(), ['daysleft' => $daysleft]);
                    return response()->json([
                        'status' => 1,
                        'subscription' => $subscribe,
                        'payment' => 3
                    ]);
                }
                else {
                    $payment = Payment::where('user_id', Auth::id())->where('active', 2)->first();
                    if (isset($payment)){
                        $payment->active = 3;
                        $payment->save();
                    }
                    return response()->json([
                        'status' => 0,
                        'payment' => $payment_status,
                    ]);
                }
            }
            else {
                return response()->json([
                    'status' => 0,
                    'payment' => $payment_status
                ]);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
