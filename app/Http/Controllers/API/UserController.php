<?php

namespace App\Http\Controllers\API;

use App\Subscription;
use App\User;
use Auth;
use Carbon\Carbon;
use File;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (isset(Auth::user()->subscription)) {
            if (isset(Auth::user()->subscription->end_date)) {
                if (strtotime(Carbon::now()->format(config('app.date_format'))) <= strtotime(Carbon::createFromFormat(config('app.date_format'), Auth::user()->subscription->end_date))) {
                    $subscription = array('subscribe' => 1);
                } else $subscription = array('subscribe' => 0);
            } else $subscription = array('subscribe' => 0);
        } else {
            $subscription = array('subscribe' => 0);
        }
        $user = Auth::user()->toArray();
        $merge = array_merge($user, $subscription);
        return response()->json($merge);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user = Auth::user();
        if ($request->has(['oldpass', 'newpass'])) {
            if (Hash::check($request->oldpass, $user->password)) {
                $user->password = bcrypt($request->newpass);
            } else {
                return response()->json([
                    'status' => 0,
                    'message' => 'Password lama tidak sesuai'
                ]);
            }
        } else {
            $user->name = $request->name;
            $user->alamat = $request->alamat;
            $user->gender = $request->gender;
            $user->no_hp = $request->no_hp;
        }
        $saved = $user->save();
        if (!$saved) {
            return response()->json([
                'status' => 0,
                'message' => 'Terdapat kesalahan dari sistem'
            ]);
        } else {
            return response()->json([
                'status' => 1,
                'message' => 'Berhasil menyimpan profil'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            return response()->json([
                "status" => 0,
                "id" => $user->id,
                "nama" => $user->name,
                "type" => 1,
                "session_token" => $user->createToken(Str::random('32'))->accessToken
            ]);
        }
        elseif (Auth::guard('admin_tutor')->attempt($credentials)) {
            $user = Auth::guard('admin_tutor')->user();
            return response()->json([
                "status" => 0,
                "id" => $user->id,
                "nama" => $user->name,
                "type" => 2,
                "session_token" => $user->createToken(Str::random('32'))->accessToken
            ]);
        }
        else {
            return response()->json([
                "status" => 1,
                "message" => "Email atau password salah."
            ]);
        }
    }

    public function isLoggedIn()
    {
        if (Auth::check()) {
            $code = "0";
            $message = null;
        }
        if (!Auth::check()) {
            $code = "1";
            $message = "Anda telah dikeluarkan dari sistem.";
        }
        $trial = false;
        if (isset(Auth::user()->subscription)) {
            if (!empty(Auth::user()->subscription->end_date) && strtotime(Carbon::now()) <= strtotime(Carbon::createFromFormat(config('app.date_format'), Auth::user()->subscription->end_date))) {
                $subscription = 1;
                if (Auth::user()->subscription->type_subs == 1) {
                    $trial = true;
                }
            } else $subscription = 0;
        } else {
            $subscription = 0;
        }
        return response()->json([
            "code" => $code,
            "subscribe" => $subscription,
            "message" => $message,
            "trial" => $trial
        ]);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'no_hp' => ['required', 'string', 'max:13', 'unique:users'],
                'password' => ['required', 'string', 'min:6', 'confirmed'],
            ],
            [
                'name.required' => 'Kolom nama harus di isi',
                'email.required' => 'Kolom email harus di isi',
                'email.unique' => 'Email sudah digunakan',
                'no_hp.required' => 'Kolom nomor hp harus di isi',
                'no_hp.unique' => 'Nomor hp sudah digunakan',
                'password.required' => 'Kolom password harus di isi',
                'password.confirmed' => 'Password tidak sama',
            ]);
        if ($validator->passes()) {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'no_hp' => $request->no_hp,
                'lembaga' => 158931,
                'password' => bcrypt($request->password),
            ]);
            $date = Carbon::now();
            Subscription::updateOrCreate(
                ['uid' => $user->id],
                [
                    'start_date' => $date->format(config('app.date_format')),
                    'end_date' => Carbon::now()->addDays(1)->format(config('app.date_format')),
                    'type_subs' => 1
                ]
            );
            return response()->json([
                "status" => 1,
                "message" => "Pendaftaran berhasil."
            ]);

        } else {
            return response()->json([
                "status" => 2,
                "message" => "Pendaftaran gagal: " . implode(", ", $validator->errors()->all())
            ]);
        }
    }

    public function upload(Request $request)
    {
        $user = Auth::user();
        if (isset($user->pp)) File::delete(public_path("storage/$user->pp"));
        $user->pp = $request->pic->store('pp');
        $saved = $user->save();
        if ($saved) {
            return response()->json([
                'status' => 1,
                'userpp' => $user->pp
            ]);
        } else {
            return response()->json([
                'status' => 0,
            ]);
        }

    }
}
