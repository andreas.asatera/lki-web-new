<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    //
    protected $fillable = [
        'uid', 'cid','score',
    ];

    public function kategori(){
        return $this->hasOne('App\Kategori', 'id', 'cid');
    }
}
