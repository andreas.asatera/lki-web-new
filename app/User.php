<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;

    public $incrementing = true;
    public $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'gender', 'tgl_lahir', 'alamat', 'no_hp', 'id_orut', 'id_kelas', 'id_sekolah', 'start_subscription', 'end_subscription', 'password', 'lembaga',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function subscription(){
        return $this->hasOne('App\Subscription', 'uid', 'id');
    }

    public function rel_lembaga(){
        return $this->hasOne('App\Lembaga', 'kode', 'lembaga');
    }

    public function score()
    {
        return $this->hasMany('App\Score', 'uid', 'id');
    }
}
