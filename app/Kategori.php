<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    //
    protected $table = 'kategoris';

    protected $fillable = ['name, type_subs'];

    public function materi(){
        return $this->hasOne('App\Materi', 'kategori', 'id');
    }

    public function soal(){
        return $this->hasOne('App\Soal', 'categoryID', 'id');
    }

    public function score(){
        return $this->hasMany('App\Score', 'cid', 'level');
    }

    public function score_user(){
        return $this->hasMany('App\Score', 'cid', 'level');
    }
}
