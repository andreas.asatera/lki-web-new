<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materi extends Model
{
    //
    public function category(){
        return $this->belongsTo('App\Kategori', 'kategori');
    }
}