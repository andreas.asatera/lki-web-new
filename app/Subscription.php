<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    //

    protected $table = 'subscriptions';
    protected $fillable = [
        'uid', 'start_date', 'end_date', 'type_subs'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'uid', 'id');
    }

    public function setEndDateAttribute($value)
    {
        $this->attributes['end_date'] = !empty($value) ? Carbon::createFromFormat(config('app.date_format'), $value)->format('Y-m-d') : null;
    }

    public function getEndDateAttribute($value)
    {
        return is_null($value) ? null : Carbon::parse($value)->format(config('app.date_format'));
    }

    public function setStartDateAttribute($value)
    {
        $this->attributes['start_date'] = !empty($value) ? Carbon::createFromFormat(config('app.date_format'), $value)->format('Y-m-d') : null;
    }

    public function getStartDateAttribute($value)
    {
        return is_null($value) ? null : Carbon::parse($value)->format(config('app.date_format'));
    }

    public function payment(){
        return $this->hasMany('App\Payment', 'user_id', 'uid');
    }
}
