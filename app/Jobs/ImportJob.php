<?php

namespace App\Jobs;

use App\Subscription;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use League\Csv\Reader;
use App\User;
use File;

class ImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $filename, $lembaga, $date_end;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($filename, $lembaga, $date_end = null)
    {
        $this->filename = $filename;
        $this->lembaga = $lembaga;
        $this->date_end = $date_end;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $csv = Reader::createFromPath(public_path('storage/import/' . $this->filename), 'r');

        $csv->setHeaderOffset(0);

        //LOOPING DATA YANG TELAH DI-LOAD
        foreach ($csv as $row) {

            //SIMPAN KE
            // DALAM TABLE USER
            $user = User::updateOrCreate(
                ['email' => $row['email']],
                [
                    'lembaga' => $this->lembaga,
                    'nomor_induk' => $row['nomor_induk'],
                    'name' => $row['nama'],
                    'no_hp' => $row['no_hp'],
                    'password' => bcrypt('123456'),
                ]
            );
            if ($this->date_end != null){
                Subscription::updateOrCreate(
                    ['uid' => $user->id],
                    [
                        'start_date' => Carbon::now()->format(config('app.date_format')),
                        'end_date' => $this->date_end
                    ]
                );
            }
        }
        File::delete(public_path('storage/import/' . $this->filename));
    }
}
