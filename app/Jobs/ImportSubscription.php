<?php

namespace App\Jobs;

use App\Subscription;
use App\User;
use Carbon\Carbon;
use File;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use League\Csv\Reader;

class ImportSubscription implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $filename, $start_date, $end_date, $type_subs;

    /**
     * ImportSubscription constructor.
     * @param $filename
     * @param $start_date
     * @param $end_date
     * @param $type_subs
     */
    public function __construct($filename, $start_date, $end_date, $type_subs)
    {
        $this->filename = $filename;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->type_subs = $type_subs;
    }


    /**
     * Create a new job instance.
     *
     * @return void
     */


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $csv = Reader::createFromPath(public_path('storage/import/' . $this->filename), 'r');

        $csv->setHeaderOffset(0);

        //LOOPING DATA YANG TELAH DI-LOAD
        foreach ($csv as $row) {
            $user = User::where('email', $row['email'])->first();
            if (isset($user)){
                Subscription::updateOrCreate(
                    ['uid' => $user->id],
                    [
                        'start_date' => $this->start_date,
                        'end_date' => $this->end_date,
                        'type_subs' => $this->type_subs,
                    ]
                );
            }
        }
        File::delete(public_path('storage/import/' . $this->filename));
    }
}
