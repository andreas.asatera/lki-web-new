<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Required meta tags -->

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="Bootstrap, Landing page, Template, Registration, Landing">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="Grayrids">
    <title>Laboratorium Kampung Inggris</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('landing-page/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('landing-page/css/line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('landing-page/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('landing-page/css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('landing-page/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('landing-page/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('landing-page/css/nivo-lightbox.css') }}">
    <link rel="stylesheet" href="{{ asset('landing-page/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('landing-page/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('landing-page/css/mfb.css') }}">
    <link rel="stylesheet" href="{{ asset('landing-page/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('landing-page/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('landing-page/css/kc.fab.css') }}"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

</head>

<body>

<!-- Header Section Start -->
<header id="home" class="hero-area-2">
    <div class="overlay"></div>
    <nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar" id="header" >
        <div class="container">
            <!--          <a href="index.html" class="navbar-brand"><img src="{{ asset('landing-page/img/img-bantuin/logo1mini.png') }}" alt=""></a>-->
            <a href="{{ url('/') }}" class="navbar-brand"><img src="{{ asset('landing-page/img/img-bantuin/LKI1.png') }}" alt="" id="gambarputih" width="100px"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="lni-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto w-100 justify-content-end">
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#home"><b>Beranda</b></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#app-features"><b>Fitur</b></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#screenshots"><b>Tampilan Aplikasi</b></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#download"><b>Download</b></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#testimonial"><b>Testimonial</b></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#pricing"><b>Berlangganan</b></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#blog"><b>Artikel</b></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#footer"><b>Kontak</b></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container" style="margin-top:100px">
        <div class="row ">

            <div class="col-lg-6 col-md-12 col-xs-12">
                <div class="intro-img">
                    <img src="{{ asset('landing-page/img/img-bantuin/Group4.png') }}" alt="" >
                </div>
            </div>

            <div class="col-lg-6 col-md-12 col-xs-12">
                <div class="contents">
                    <div style="text-align: center;">
                        <h2 class="head-title">Hai, Selamat Datang di</h2>

                        <!--<div class="col-lg-5 col-md-12 col-xs-12">-->
                        <!--                <div class="intro-img">-->
                        <img src="{{ asset('landing-page/img/img-bantuin/LKI1.png') }}" alt="" style="width:100%">
                        <!--                </div>-->
                        <!--</div>-->

                        <div></div>
                        <!--              <p>Bantuin Kerja, Solusi cepat dapat kerja.</p>-->
                        <p>It's Time To Learn English.</p>
                        <div class="header-button">
                            <!--                <a href="#" class="btn btn-border-filled">Learn More</a>-->
                            <!--                <a href="#" class="btn btn-border">Get Started</a>-->
                        </div>
                    </div>
                </div>
            </div>

        </div><!--row-->
    </div>
</header>

<!--Carousel Wrapper-->
<div id="features-two" class="section">
    <div class="container">

        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="img-thumb wow fadeInLeft" data-wow-delay="0.3s">
                    <img class="img-fluid" src="{{ asset('landing-page/img/img-bantuin/ss1-01%201.png') }}" alt="" width="100%">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="text-wrapper wow fadeInRight" data-wow-delay="0.6s">
                    <div>
                        <!-- <p class="btn btn-subtitle">How It Works?</p> -->
                        <h3>Bingung mempelajari Bahasa Inggris?</h3>
                        <p>Mulailah untuk mempelajari materi yang disajikan dalam bentuk teks dan video.Yuk belajar Bahasa Inggris dengan mudah dan menyenangkan di Aplikasi Laboratorium Kampung Inggris.Dan uji pengetahuan kamu dengan mengerjakan soal di Online Quiz</p>
                        <!--  <a class="btn btn-rm" href="#">Read More <i class="lni-arrow-right"></i></a> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="img-thumb wow fadeInLeft" data-wow-delay="0.3s">
                    <img class="img-fluid" src="{{ asset('landing-page/img/img-bantuin/ss1-02%201.png') }}" alt="" width="100%">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="text-wrapper wow fadeInRight" data-wow-delay="0.6s">
                    <div>
                        <!-- <p class="btn btn-subtitle">How It Works?</p> -->
                        <h3>Orang tua dan guru bisa mengetahui perkembangan siswa dan anak</h3>
                        <p>Karena di Aplikasi LKI ada Log in Guru dan Orang tua dimana bisa melihat perkembangan anak didik antara lain bisa melihat nilai Quiz dan sebagainya.</p>
                        <!-- <a class="btn btn-rm" href="#">Read More <i class="lni-arrow-right"></i></a> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="img-thumb wow fadeInLeft" data-wow-delay="0.3s">
                    <img class="img-fluid" src="{{ asset('landing-page/img/img-bantuin/ss1-03%201.png') }}" alt="" width="100%">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="text-wrapper wow fadeInRight" data-wow-delay="0.6s">
                    <div>
                        <!-- <p class="btn btn-subtitle">How It Works?</p>  -->
                        <h3>Memberikan layanan chat dengan mentor.</h3>
                        <p>Aplikasi Laboratorium Kampung Inggris akan memberikan layanan chat dengan mentor yang khusus untuk membahas semua tentang pembelajaran Bahasa Inggris.</p>
                        <!-- <a class="btn btn-rm" href="#">Read More <i class="lni-arrow-right"></i></a> -->
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!--/.Carousel Wrapper-->

<!-- features Section Start -->
<div id="app-features" class="section">
    <div class="container">
        <div class="section-header">
            <p class="btn btn-subtitle wow fadeInDown" data-wow-delay="0.2s">Fitur</p>
            <h2 class="section-title wow fadeIn" data-wow-delay="0.2s">Fitur Aplikasi</h2>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-12 col-xs-12">
                <div class="content-left text-right">
                    <div class="box-item left">
                <span class="icon">
                  <i class="lni-film-play"></i>
                </span>
                        <div class="text">
                            <h4>E-Learn Video</h4>
                            <p>Pelajari semua materi Bahasa Inggris dalam bentuk video dan teks di menu E-Learn Video.</p>
                        </div>
                    </div>
                    <div class="box-item left">
                <span class="icon">
                  <i class="lni-pencil"></i>
                </span>
                        <div class="text">
                            <h4>Online Quiz</h4>
                            <p>Uji pengetahuan kamu dengan menjawab soal quiz dan ketahui hasilnya.</p>
                        </div>
                    </div>
                    <div class="box-item left">
                <span class="icon">
                  <i class="lni-medall-alt"></i>
                </span>
                        <div class="text">
                            <h4>E-Score</h4>
                            <p>Selesai mengerjaan Online Quiz ketahui nilaimu di menu E-Score.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-xs-12">
                <div class="show-box">
                    <img src="{{ asset('landing-page/img/img-bantuin/Group3.png') }}" alt="" style="width:100%; height:400px" >
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-xs-12">
                <div class="content-right text-left">
                    <div class="box-item right">
                <span class="icon">
                  <i class="lni-notepad"></i>
                </span>
                        <div class="text">
                            <h4>E-Task</h4>
                            <p>Menu untuk mengerjakan soal Task yang diberikan oleh mentor.</p>
                        </div>
                    </div>
                    <div class="box-item right">
                <span class="icon">
                  <i class="lni-wordpress"></i>
                </span>
                        <div class="text">
                            <h4>E-News</h4>
                            <p>Tersedia artikel-artikel yang dapat membantumu tentang materi Bahasa Inggris.</p>
                        </div>
                    </div>
                    <div class="box-item right">
                <span class="icon">
                  <i class="lni-comments-alt"></i>
                </span>
                        <div class="text">
                            <h4>Discussion</h4>
                            <p>Kamu dapat melakukan diskusi via chat dengan mentor.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- features Section End -->

<!-- Start Video promo Section -->
<section class="video-promo section">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-12"></div>
            <div class="col-md-6 col-sm-12">
                <div class="video-promo-content text-center">
                    <!-- <a href="https://youtu.be/D4PTiJiJeqg" class="video-popup"><i class="lni-film-play"></i></a> -->
                    <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/YBf6eNV_gLQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                    <video controls poster="{{ asset('landing-page/img/img-bantuin/LKI1.png') }}" style=" width:100%; max-height:300px; min-height:300px;">
                        <source src="{{ asset('storage/video/Video_7_Negative_Contraction.mp4') }}" type="video/mp4">
                    </video>
                    <h2 class="mt-3 wow zoomIn" data-wow-duration="1000ms" data-wow-delay="100ms">Video E-Learn</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Video Promo Section -->

<!-- Awesome Screens Section Start -->
<section id="screenshots" class="screens-shot section">
    <div class="container">
        <div class="section-header">
            <p class="btn btn-subtitle wow fadeInDown" data-wow-delay="0.2s">Screenshots</p>
            <h2 class="section-title">Tampilan Aplikasi</h2>
        </div>
        <div class="row">
            <div class="touch-slider owl-carousel">
                <div class="item">
                    <div class="screenshot-thumb">
                        <img class="img-fluid" src="{{ asset('landing-page/img/img-bantuin/unnamed.jpg') }}" alt="">
                    </div>
                </div>
                <div class="item">
                    <div class="screenshot-thumb">
                        <img class="img-fluid" src="{{ asset('landing-page/img/img-bantuin/unnamed (1).jpg') }}" alt="">
                    </div>
                </div>
                <div class="item">
                    <div class="screenshot-thumb">
                        <img class="img-fluid" src="{{ asset('landing-page/img/img-bantuin/unnamed (2).jpg') }}" alt="">
                    </div>
                </div>
                <div class="item">
                    <div class="screenshot-thumb">
                        <img class="img-fluid" src="{{ asset('landing-page/img/img-bantuin/unnamed (3).jpg') }}" alt="">
                    </div>
                </div>
                <div class="item">
                    <div class="screenshot-thumb">
                        <img class="img-fluid" src="{{ asset('landing-page/img/img-bantuin/unnamed (4).jpg') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Awesome Screens Section End -->

<!-- download Section Start -->
<section id="download">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="download-thumb wow fadeInLeft" data-wow-delay="0.2s">
                    <img class="img-fluid" src="{{ asset('landing-page/img/img-bantuin/Group 2.png/') }}" alt="">
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="download-wrapper wow fadeInRight" data-wow-delay="0.2s">
                    <div>
                        <div class="download-text">
                            <h4>Download Aplikasi dari Play Store</h4>
                            <p>Laboratorium Kampung Inggris adalah media pembelajaran Bahasa Inggris berbasis Online.Aplikasi ini merupakan media pembelajaran yang interaktif untuk digunakan siswa,berisikan konten edukasi yang dikemas secara menarik oleh tentor profesional dari Lembaga Bimbingan Bahasa Inggris Elfast.Aplikasi ini juga dilengkapi fitur bagi orang tua untuk mengetahui kemajuan perkembangan siswa.</p><br>
                            <p>Berikut adalah cara berlangganan Aplikasi Bantuin Kerja</p>
                        </div>
                        <a href="#" class="btn btn-common btn-effect"><i class="lni-android"></i>Download From PlayStore<br></a>
                        <a href="#" class="btn btn-apple">1.Klik Subscription<br></a>
                        <a href="#" class="btn btn-apple">2.Klik tombol Langganan<br></a>
                        <a href="#" class="btn btn-apple">3.Mendapatkan Kode Bayar<br></a>
                        <a href="#" class="btn btn-apple">4.Bayar melalui Bank<br></a>
                        <a href="#" class="btn btn-apple">5.Upload bukti pembayaran<br></a>
                        <a href="#" class="btn btn-apple">6.Akun aktif dan digunakan<br> setelah terverifikasi<br></a><br><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- download Section Start -->

<!-- Testimonial Section Start -->
<section id="testimonial" class="section">
    <div class="container">

        <div class="section-header">
            <p class="btn btn-subtitle wow fadeInDown" data-wow-delay="0.2s">Reviews</p>
            <h2 class="section-title">Testimoni dari Pengguna</h2>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">

                <div id="testimonials" class="touch-slider owl-carousel">

                    <div class="item">
                        <div class="testimonial-item">
                            <div class="author">
                                <div class="img-thumb">
                                    <img src="{{ asset('landing-page/img/img-bantuin/Untitled-2-01.png') }}" alt="" width="100%">
                                </div>
                            </div>
                            <div class="content-inner">
                                <p class="description">Aplikasi LKI ini sangat direkomendasikan untuk kalian yang belum paham dengan bahasa inggris.</p>
                                <div class="author-info">
                                    <h2><a href="#">Alif Firdaus</a></h2>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-item">
                            <div class="author">
                                <div class="img-thumb">
                                    <img src="{{ asset('landing-page/img/img-bantuin/Untitled-2-01.png') }}" alt="" width="100%">
                                </div>
                            </div>
                            <div class="content-inner">
                                <p class="description">Aplikasi ini sangat berguna apabila kita ingin belajar bahasa inggris dengan lebih baik lagi</p>
                                <div class="author-info">
                                    <h2><a href="#">Andry Dony</a></h2>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-item">
                            <div class="author">
                                <div class="img-thumb">
                                    <img src="{{ asset('landing-page/img/img-bantuin/Untitled-2-01.png') }}" alt="" width="100%">
                                </div>
                            </div>
                            <div class="content-inner">
                                <p class="description">Aplikasi ini sangat membantu dalam proses belajar bahasa Inggris.Karena materi mudah dipahami.</p>
                                <div class="author-info">
                                    <h2><a href="#">Gatut Sugiantoro</a></h2>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-item">
                            <div class="author">
                                <div class="img-thumb">
                                    <img src="{{ asset('landing-page/img/img-bantuin/Untitled-2-01.png') }}" alt="" width="100%">
                                </div>
                            </div>
                            <div class="content-inner">
                                <p class="description">Aplikasi LKI ini sangat membantu saya mempelajari Bahasa Inggris dengan baik dalam waktu yang tidak lama.</p>
                                <div class="author-info">
                                    <h2><a href="#">Afdhol</a></h2>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-item">
                            <div class="author">
                                <div class="img-thumb">
                                    <img src="{{ asset('landing-page/img/img-bantuin/Untitled-2-02.png') }}" alt="" width="100%">
                                </div>
                            </div>
                            <div class="content-inner">
                                <p class="description">Aplikasi LKI mudah digunakan,soal E-Task up to date.Top deh pokoknya.Segera download teman-teman.</p>
                                <div class="author-info">
                                    <h2><a href="#">Endria Wahyuning Anindra Putri</a></h2>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-item">
                            <div class="author">
                                <div class="img-thumb">
                                    <img src="{{ asset('landing-page/img/img-bantuin/Untitled-2-01.png') }}" alt="" width="100%">
                                </div>
                            </div>
                            <div class="content-inner">
                                <p class="description">Aplikasi LKI mudah digunakan,materi mudah dipahami.Segera download ya teman-teman.</p>
                                <div class="author-info">
                                    <h2><a href="#">R.Dhimas Nur Febrianto</a></h2>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-item">
                            <div class="author">
                                <div class="img-thumb">
                                    <img src="{{ asset('landing-page/img/img-bantuin/Untitled-2-02.png') }}" alt="" width="100%">
                                </div>
                            </div>
                            <div class="content-inner">
                                <p class="description">Dengan aplikasi LKI memudahkan saya mengerjakan soal Bahasa Inggris.Serta mengetahui berapa salah dan benar jawaban saya.</p>
                                <div class="author-info">
                                    <h2><a href="#">Siti Nur Hanifah</a></h2>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-item">
                            <div class="author">
                                <div class="img-thumb">
                                    <img src="{{ asset('landing-page/img/img-bantuin/Untitled-2-01.png') }}" alt="" width="100%">
                                </div>
                            </div>
                            <div class="content-inner">
                                <p class="description">Aplikasi ini membantu saya dalam belajar Bahasa Inggris.Jadi tak takut kalau mengerjakan soal Bahasa Inggris.</p>
                                <div class="author-info">
                                    <h2><a href="#">Yudha Aldila Efendi</a></h2>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-item">
                            <div class="author">
                                <div class="img-thumb">
                                    <img src="{{ asset('landing-page/img/img-bantuin/Untitled-2-02.png') }}" alt="" width="100%">
                                </div>
                            </div>
                            <div class="content-inner">
                                <p class="description">Setiap mengerjakan soal Bahasa Inggris selalu gagal berkat Aplikasi LKI saya bisa mengerjakan soal bahasa inggris.Terima kasih Bantuin Kerja.</p>
                                <div class="author-info">
                                    <h2><a href="#">Serlii Ratmala Crusita</a></h2>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-item">
                            <div class="author">
                                <div class="img-thumb">
                                    <img src="{{ asset('landing-page/img/img-bantuin/Untitled-2-02.png') }}" alt="" width="100%">
                                </div>
                            </div>
                            <div class="content-inner">
                                <p class="description">Soal Bahasa Inggris LKI bervariatif,mudah dioperasikan dan sangat membantu untuk fresh graduate.Terima kasih Bantuin Kerja.</p>
                                <div class="author-info">
                                    <h2><a href="#">Nurlinda Rifqia Zuharoh</a></h2>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</section>
<!-- Testimonial Section End -->

<!-- Start Pricing Table Section -->
<div id="pricing" class="section pricing-section" style="background-color:#eefdff;" >
    <div class="container">
        <div class="section-header">
            <p class="btn btn-subtitle wow fadeInDown" data-wow-delay="0.2s">Pelatihan</p>
            <h2 class="section-title">Harga Paket Latihan</h2>
        </div>

        <div class="row pricing-tables">
            <!-- <div class="col-lg-4 col-md-4 col-xs-12">
              <div class="pricing-table wow fadeInLeft" data-wow-delay="0.2s">
                <div class="pricing-details">
                  <div class="icon">
                    <i class="lni-package"></i>
                  </div>
                  <h2>Trial</h2>
                  <ul>
                    <li>Waktu berlangganan 3 Hari</li>
                    <li>Akses Uji psikotes 5x</li>
                    <li>Dapat mengetahui nilai IQ</li>
                    <li>Latihan Psikotes disertai pembahasan soal</li>
                    <li>Mendapatkan 1 template CV Premium</li>
                  </ul>
                  <div class="price">Rp.10.000,-/3hari<span></span></div>
                </div>
                <div class="plan-button">
                  <a href="https://api.whatsapp.com/send?phone=6285706133736&amp;text=Halo%20admin%20Bantuin%20Kerja,%20Saya%20ingin%20berlangganan%20paket%20Trial%20" class="btn btn-border">Berlangganan</a>
                </div>
              </div>
            </div> -->
            <div class="col-lg-4 col-md-4 col-xs-12"></div>
            <div class="col-lg-4 col-md-4 col-xs-12">
                <div class="pricing-table wow fadeInLeft" data-wow-delay="0.2s">
                    <div class="pricing-details">
                        <div class="icon">
                            <i class="lni-drop"></i>
                        </div>
                        <h2>1 Tahun</h2>
                        <ul>
                            <li>100 video pembelajaran</li>
                            <li>100 materi pembelajaran</li>
                            <li>100 latihan soal</li>
                        </ul>
                        <div class="price">Rp.100.000,-/Tahun<span></span></div>
                    </div>
                    <div class="plan-button">
                        <a href="https://api.whatsapp.com/send?phone=6285706133736&amp;text=Halo%20admin%20Bantuin%20Kerja,%20Saya%20ingin%20berlangganan%20paket%20Basic%20" class="btn btn-border">Berlangganan</a>
                    </div>
                </div>
            </div>

            <!-- <div class="col-lg-4 col-md-4 col-xs-12">
              <div class="pricing-table">
                <div class="pricing-details">
                  <div class="icon">
                    <i class="lni-briefcase"></i>
                  </div>
                  <h2>Profesional</h2>
                  <ul>
                    <li>Waktu berlangganan 1 Bulan</li>
                    <li>Akses Uji psikotes 60x</li>
                    <li>Dapat mengetahui nilai IQ</li>
                    <li>Latihan Psikotes disertai pembahasan soal</li>
                    <li>Mendapatkan 5 template CV Premium</li>
                  </ul>
                  <div class="price">Rp.50.000,-/Bulan<span></span></div>
                </div>
                <div class="plan-button">
                  <a href="https://api.whatsapp.com/send?phone=6285706133736&amp;text=Halo%20admin%20Bantuin%20Kerja,%20Saya%20ingin%20berlangganan%20paket%20Profesional%20" class="btn btn-border">Berlangganan</a>
                </div>
              </div>
            </div> -->

        </div>
    </div>
</div>
<!-- End Pricing Table Section -->


<!-- Blog Section -->
<section id="blog" class="section" style="background-color:#b7d4fd;">
    <!-- Container Starts -->
    <div class="container">
        <div class="section-header">
            <p class="btn btn-subtitle wow fadeInDown" data-wow-delay="0.2s">Artikel</p>
            <h2 class="section-title">Berita Terbaru</h2>
        </div>
        <div class="row">

            <div class="col-lg-4 col-md-6 col-xs-12 blog-item">
                <!-- Blog Item Starts -->
                <div class="blog-item-wrapper">
                    <div class="blog-item-img">
                        <a href="">
                            <img src="{{ asset('landing-page/img/img-bantuin/LKI1.png') }}" alt="" width="100%">
                        </a>
                    </div>

                    <div class="blog-item-text">
                        <h3><a href="">Petunjuk penggunaan Aplikasi Laboratorium Kampung Inggris</a></h3>
                        <div class="author">
                            <span class="name"><a href="#">Posted by Admin</a></span>
                            <span class="date float-right"></span>
                        </div>
                    </div>
                </div>
                <!-- Blog Item Wrapper Ends-->
            </div>

            <div class="col-lg-4 col-md-6 col-xs-12 blog-item">
                <!-- Blog Item Starts -->
                <div class="blog-item-wrapper">
                    <div class="blog-item-img">
                        <a href="">
                            <img src="{{ asset('landing-page/img/img-bantuin/LKI1.png') }}" alt="" width="100%">
                        </a>
                    </div>

                    <div class="blog-item-text">
                        <h3><a href="#">Press Release Aplikasi Laboratorium Kampung Inggris</a></h3>
                        <div class="author">
                            <span class="name"><a href="#">Posted by Admin</a></span>
                            <span class="date float-right"></span>
                        </div>
                    </div>
                </div>
                <!-- Blog Item Wrapper Ends-->
            </div>

            <div class="col-lg-4 col-md-6 col-xs-12 blog-item">
                <!-- Blog Item Starts -->
                <div class="blog-item-wrapper">
                    <div class="blog-item-img">
                        <a href="#">
                            <img src="{{ asset('landing-page/img/img-bantuin/LKI1.png') }}" alt="" width="100%">
                        </a>
                    </div>

                    <div class="blog-item-text">
                        <h3><a href="#">Laboratorium Kampung Inggris Launching tanggal 1112020</a></h3>
                        <div class="author">
                            <span class="name"><a href="#">Posted by Admin</a></span>
                            <span class="date float-right"></span>
                        </div>
                    </div>
                </div>
                <!-- Blog Item Wrapper Ends-->
            </div>

            <!-- <div class="col-lg-4 col-md-6 col-xs-12 blog-item">

                  <div class="blog-item-wrapper">
                      <div class="blog-item-img">
                          <a href="galeri.html">
                              <img src="{{ asset('landing-page/img/img-bantuin/logo12-kecil.png') }}" alt="">
                          </a>
                      </div>

                      <div class="blog-item-text">
                          <h3><a href="galeri.html">Galeri Bantuin Kerja</a></h3>
                          <div class="author">
                              <span class="name"><a href="#galeri">Posted by Admin</a></span>
                              <span class="date float-right"></span>
                          </div>
                      </div>
                  </div>

            </div> -->

        </div>
    </div>
</section>
<!-- blog Section End -->

<!-- Map Section Start -->
<section id="map-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 padding-0">
                <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3952.8466043912667!2d111.99903911470342!3d-7.806058679659609!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7857333577fc79%3A0x4c196f90fb0877e7!2sBEADGRUP%20HEAD%20OFFICE!5e0!3m2!1sid!2sid!4v1575457380647!5m2!1sid!2sid" width="100%" height="450px" frameborder="0" style="border:0;" allowfullscreen=""></iframe> -->
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.3116886996722!2d112.178014314326!3d-7.756729679068022!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e785c4f66e56ccb%3A0xb1c3cd848a8f1989!2sElfast!5e0!3m2!1sid!2sid!4v1576198135006!5m2!1sid!2sid" width="100%" height="450px" style="border:0;" allowfullscreen=""></iframe>
            </div>
        </div>
    </div>
</section>
<!-- Map Section End -->

<!-- Floating Button -->
<div class="kc_fab_wrapper" >

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="{{ asset('landing-page/js/kc.fab.min.js') }}"></script>
<script>
    $(document).ready(function(){
        var links = [
            {
                "bgcolor":"#00bfa5",
                "icon":"<i class='fa fa-comment-o'></i>"
            },
            {
                "url":"https://api.whatsapp.com/send?phone=628113669566&amp;text=Halo%20admin%20Laboratorium%20Kampung%20Inggris,%20Saya%20ingin%20bertanya%20",
                "bgcolor":"#00bfa5",
                "color":"#fffff",
                "title":"Chat Whatsapp",
                "icon":"<i class='fa fa-whatsapp'></i>",
            },
            {
                "url":"https://www.instagram.com/bantuin.kerja/?hl=id target=_blank",
                "bgcolor":"#833AB4",
                "color":"white",
                "title":"Follow @bantuin.kerja",
                "icon":"<i class='fa fa-instagram'></i>"
            },
            {
                "url":"info@beadgrup.com",
                "bgcolor":"#D44638",
                "color":"white",
                "title":"Mail to Beadgrup",
                "icon":"<i class='fa fa-envelope'></i>"
            }
        ];
        $('.kc_fab_wrapper').kc_fab(links);
    })
</script>
<!-- Floating Button -->

<!-- Footer Section Start -->
<footer>
    <!-- Footer Area Start -->
    <section id="footer" class="footer-Content">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
                    <img src="{{ asset('landing-page/img/img-bantuin/LKI1.png') }}" alt="" width="100%">
                    <div class="textwidget">
                        <p>LABORATORIUM KAMPUNG INGGRIS adalah media pembelajaran Bahasa Inggris berbasis Online.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
                    <div class="widget">
                        <h3 class="block-title" style="font-size:15px;">LABORATORIUM KAMPUNG INGGRIS</h3>
                        <ul class="menu">
                            <!--                  <li><a href="#">Sign In</a></li>-->
                            <!--                  <li><a href="#">About Us</a></li>-->
                            <!--                  <li><a href="#">Pricing</a></li>-->
                            <!--                  <li><a href="#">Jobs</a></li>-->
                            <p  class="text-justify"> <a style="text-decoration: none; color:#fff; font-size:15px; " href="">
                                    LABORATORIUM KAMPUNG INGGRIS adalah media pembelajaran Bahasa Inggris berbasis Online.Aplikasi ini merupakan media pembelajaran yang interaktif untuk digunakan siswa,berisikan konten edukasi yang dikemas secara menarik oleh tentor profesional dari Lembaga Bimbingan Bahasa Inggris Elfast.</a></p>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12" style="font-size:20px;">
                    <div class="widget">
                        <h3 class="block-title" style="font-size:15px;">Contact us</h3>
                        <ul class="menu">
                            <!--                  <li><a href="#"></a></li>-->
                            <!--                  <li><a href="#"></a></li>-->
                            <!--                  <li><a href="#"></a></li>-->
                            <!--                  <li><a href="#"></a></li>-->
                            <p> <a style="text-decoration: none; color:#fff; font-size:15px; " ><span class="fa fa-phone"></span> 0811-3669-566</a></p>
                            <p>  <a style="text-decoration: none; color:#fff; font-size:15px;" href="mailto:info@beadgrup.com"> <span class="fa fa-envelope"></span> info@beadgrup.com</a></p>
{{--                            <p><a style="text-decoration: none; color:#fff; font-size:15px;" href="https://www.instagram.com/bantuin.kerja/" target="_blank"><span class="fa fa-instagram"></span> @bantuin.kerja</a></p>--}}
{{--                            <p><a style="text-decoration: none; color:#272525; font-size:15px;" href="galeri.html" target="_blank"><span class="fa fa-film"></span> Galeri Bantuin Kerja</a></p>--}}
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12" style="font-size:15px;">
                    <div class="widget">
                        <h3 class="block-title" style="font-size:15px;">Kantor Pusat</h3>
                        <ul class="menu">
                            <!--                  <li><a href="#"></a></li>-->
                            <!--                  <li><a href="#"></a></li>-->
                            <!--                  <li><a href="#"></a></li>-->
                            <!--                  <li><a href="#"></a></li>-->
                            <p> <a style="text-decoration: none; color:#fff; font-size:15px;" href="#map-area"><span class="fa fa-home"></span> Kampung Inggris Kediri, Jl.Kemuning, Mangunrejo, Tulungrejo, Kec. Pare, Kediri, Jawa Timur 64212</a></p>

                            <p> <a class="text-justify" style="text-decoration: none; color:#fff; font-size:15px;">Senin - Jumat | 08.00 WIB - 17.00 WIB</a></p>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Copyright Start  -->
        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="site-info float-left">
                            <p>&copy; 2020 - Designed by <a href="{{ url('/') }}" rel="nofollow">Laboratorium Kampung Inggris</a></p>
                        </div>
                        <div class="float-right">
                            <ul class="footer-social">
                                <!--                    <li><a class="facebook" href="#"><i class="lni-facebook-filled"></i></a></li>-->
                                <!--                    <li><a class="twitter" href="#"><i class="lni-twitter-filled"></i></a></li>-->
                                <!--                    <li><a class="linkedin" href="#"><i class="lni-linkedin-fill"></i></a></li>-->
                                <!--                    <li><a class="google-plus" href="#"><i class="lni-google-plus"></i></a></li>-->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Copyright End -->
    </section>
    <!-- Footer area End -->

</footer>
<!-- Footer Section End -->

<!-- Go To Top Link -->
<!--    <a href="#" class="back-to-top">-->
<!--      <i class="lni-chevron-up"></i>-->
<!--    </a> -->

<!-- Preloader -->
<div id="preloader">
    <div class="loader" id="loader-1"></div>
</div>
<!-- End Preloader -->

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="{{ asset('landing-page/js/jquery-min.js') }}"></script>
<script src="{{ asset('landing-page/js/popper.min.js') }}"></script>
<script src="{{ asset('landing-page/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('landing-page/js/owl.carousel.js') }}"></script>
<script src="{{ asset('landing-page/js/jquery.mixitup.js') }}"></script>
<script src="{{ asset('landing-page/js/jquery.nav.js') }}"></script>
<script src="{{ asset('landing-page/js/scrolling-nav.js') }}"></script>
<script src="{{ asset('landing-page/js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('landing-page/js/wow.js') }}"></script>
<script src="{{ asset('landing-page/js/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('landing-page/js/nivo-lightbox.js') }}"></script>
<script src="{{ asset('landing-page/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('landing-page/js/waypoints.min.js') }}"></script>
<script src="{{ asset('landing-page/js/form-validator.min.js') }}"></script>
<script src="{{ asset('landing-page/js/contact-form-script.js') }}"></script>
<script src="{{ asset('landing-page/js/main.js') }}"></script>
<script src="{{ asset('landing-page/js/mfb/modernizr.touch.js') }}"></script>
<script src="{{ asset('landing-page/js/kc.fab.min.js') }}"></script>

</body>
</html>
