@extends('adminlte::page')

@section('title', 'Tambah Sekolah')

@section('content_header')
    <h1>Tambah Sekolah</h1>
@endsection

@section('content')
    @if ($message = Session::get('warning'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
            </button>
            {!! $message !!}
        </div>
    @endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! $message !!}
        </div>
    @endif
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Tambah Sekolah</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{ route('sekolah.store') }}" method="post">
            @csrf
            <div class="box-body">
                <div class="form-group">
                    <label for="sekolah">Nama Sekolah</label>
                    <input type="text" class="form-control" id="sekolah" placeholder="Masukkan nama sekolah" name="name" autocomplete="off" style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII='); cursor: auto;">
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">TAMBAH</button>
            </div>
        </form>
    </div>
@endsection

