@extends('adminlte::page')

@section('title', 'Tutor')

@section('content_header')

@endsection

@section('content')
    @if (($message = Session::get('success')) || ($message = Session::get('error')))
        <div class="alert alert-{{ Session::get('success') ? 'success' : 'danger'}} alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! $message !!}
        </div>
    @endif
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Tambah Tutor</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{ route('tutor.store') }}" method="post">
            @csrf
            <div class="box-body">
                <div class="form-group">
                    <label for="nama">Nama Tutor</label>
                    <input type="text" class="form-control" id="nama" placeholder="Masukkan nama tutor" name="nama" autocomplete="off" style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII='); cursor: auto;">
                </div>
                <div class="form-group">
                    <label for="nohp">No HP Tutor</label>
                    <input type="number" class="form-control" id="nohp" placeholder="Masukkan no hp tutor" name="nohp" autocomplete="off" style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII='); cursor: auto;">
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">TAMBAH</button>
            </div>
        </form>
    </div>
@endsection

