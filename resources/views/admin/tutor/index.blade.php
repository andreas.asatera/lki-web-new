@extends('adminlte::page')

@section('title', 'Tutor')

@section('content_header')
    <h1>Daftar Tutor</h1>
    @if (($message = Session::get('success')) || ($message = Session::get('error')))
        <div class="alert alert-{{ Session::get('success') ? 'success' : 'danger'}} alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! $message !!}
        </div>
    @endif
@endsection

@section('content')
    @php($start = 0)
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="tutor" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 1%">No.</th>
                                    <th>Nama Tutor</th>
                                    <th>No. HP</th>
                                    <th style="width: 10%">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($tutors as $tutor)
                                    <tr>
                                        <td>{{ ++$start }}.</td>
                                        <td>{{ $tutor->nama }}</td>
                                        <td>{{ $tutor->nohp }}</td>
                                        <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                                    data-target="#aksi-{{ $tutor->id }}">UBAH
                                            </button></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @foreach($tutors as $tutor)
        <div class="modal fade" id="aksi-{{ $tutor->id }}" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="{{ route('tutor.update', $tutor->id) }}" method="POST">
                        @method('PATCH')
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Edit Tutor: {{ $tutor->nama }}</h4>
                        </div>
                        <div class="modal-body">
                            @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="nama">Nama Tutor</label>
                                    <input type="text" class="form-control" id="nama" placeholder="Masukkan nama tutor" name="nama" autocomplete="off" style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII='); cursor: auto;" value="{{ $tutor->nama }}">
                                </div>
                                <div class="form-group">
                                    <label for="nohp">No HP Tutor</label>
                                    <input type="number" class="form-control" id="nohp" placeholder="Masukkan no hp tutor" name="nohp" autocomplete="off" style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII='); cursor: auto;"value="{{ $tutor->nohp }}">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default"
                                    data-dismiss="modal">Batal
                            </button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endsection

@section('js')
    <script>
        $(function () {
            $("#tutor").DataTable();
        });
    </script>
@stop