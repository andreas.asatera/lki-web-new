@extends('adminlte::page')

@section('title', 'Payment')

@section('content_header')
    <h1>Pembayaran User</h1>
    @if (($message = Session::get('success')) || ($message = Session::get('error')))
        <div class="alert alert-{{ Session::get('success') ? 'success' : 'danger'}} alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! $message !!}
        </div>
    @endif
@endsection

@section('content')
    @php($start = 0)
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="user" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 1%">No.</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Tgl Pembayaran</th>
                                    <th>Status</th>
                                    <th style="width: 10%">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($payments as $payment)
                                    <tr>
                                        <td>{{ ++$start }}.</td>
                                        <td>{{ $payment->user->name }}</td>
                                        <td>{{ $payment->user->email }}</td>
                                        <td>{{ $payment->created_at }}</td>
                                        <td>{!! $payment->active == 2 ? '<span class="label label-danger">Belum di verifikasi</span>' : '<span class="label label-success">Sudah di verifikasi</span>' !!}</td>
                                        <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                                    data-target="#aksi-{{ $payment->id }}">UBAH
                                            </button></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @foreach($payments as $payment)
        <div class="modal fade" id="aksi-{{ $payment->id }}" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="{{ route('payment.update', $payment->user->id) }}" method="POST">
                        @method('PATCH')
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Verifikasi Pembayaran: {{ $payment->user->name }}</h4>
                        </div>
                        <div class="modal-body">
                            @csrf
                            <div class="box-body">
                                <div>
                                    <p><b>Bukti Pembayaran</b></p>
                                    <a href="{{ asset("storage/$payment->image") }}" target="_blank"><img src="{{ asset("storage/$payment->image") }}" class="img-thumbnail" alt=""></a>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="type_subs" value="{{$payment->type_subs}}" hidden>
                                    <label for="type_subs">Type Langganan</label>
                                    <select id="type_subs" class="form-control" required disabled>
                                        <option value="2" {{ $payment->type_subs == 2 ? "selected" : "" }}>Beginner</option>
                                        <option value="3" {{ $payment->type_subs == 3 ? "selected" : "" }}>Intermediate</option>
                                        <option value="4" {{ $payment->type_subs == 4 ? "selected" : "" }}>Advance</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="datestart">Awal Berlangganan</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right"
                                               id="datestart" name="start_date" readonly
                                               value="{{ isset($payment->user->subscription) ? $payment->user->subscription->start_date : "" }}"
                                               placeholder="-">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="dateend-{{ $payment->id }}">Akhir Berlangganan</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right"
                                               id="dateend-{{ $payment->id }}" name="end_date"
                                               value="{{ isset($payment->user->subscription) ? $payment->user->subscription->end_date : "" }}"
                                               placeholder="-" autocomplete="off" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="text" name="payment_id" value="{{ $payment->id }}" hidden required>
                            <button type="button" class="btn btn-default"
                                    data-dismiss="modal">Batal
                            </button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $("#user").DataTable();
            @foreach($payments as $payment)
            $('#dateend-{{ $payment->id }}').datepicker({
                autoclose: true,
                format: '{{ config('app.date_format_javascript') }}',
                locale: 'en',
                startDate: new Date()
            }).on("changeDate", function (e) {
                $('#dateend-{{ $payment->id }}').attr('value', e.format());
            });
            @endforeach
        });
    </script>
@stop
