@extends('adminlte::page')

@section('title', 'Tambah Kategori')

@section('content_header')
    <h1>Tambah Kategori</h1>
@endsection

@section('content')
    @if (($message = Session::get('success')) || ($message = Session::get('error')))
        <div class="alert alert-{{ Session::get('success') ? 'success' : 'danger'}} alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! $message !!}
        </div>
    @endif
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Tambah Kategori</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{ route('kategori.store') }}" method="post">
            @csrf
            <div class="box-body">
                <div class="form-group">
                    <label for="kategori">Nama Kategori</label>
                    <input type="text" class="form-control" id="kategori" placeholder="Masukkan nama kategori" name="name" autocomplete="off" style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII='); cursor: auto;">
                </div>
                <div class="form-group">
                    <label for="level">Materi yang harus diselesaikan terlebih dahulu</label>
                    <select name="level" id="level" class="form-control">
                        <option value="">Tidak ditentukan</option>
                        @foreach($kategories as $kategori)
                            <option value="{{ $kategori->id }}">{{ $kategori->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">TAMBAH</button>
            </div>
        </form>
    </div>
@endsection

@section('js')
    <script>
        $(function () {
            $("#level").select2();
        });
    </script>
@stop