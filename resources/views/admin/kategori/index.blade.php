@extends('adminlte::page')

@section('title', 'Kategori')

@section('content_header')
  <h1>Daftar Kategori</h1>
  @if (($message = Session::get('success')) || ($message = Session::get('error')))
    <div class="alert alert-{{ Session::get('success') ? 'success' : 'danger'}} alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      {!! $message !!}
    </div>
  @endif
@endsection

@section('content')
  @php($start = 0)
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <table id="kategori" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="width: 1%">No.</th>
                    <th>Nama Kategori</th>
                    <th>Paket Langganan</th>
                    <th style="width: 10%">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($categories as $category)
                    <tr>
                      <td>{{ ++$start }}.</td>
                      <td>{{ $category->name }}</td>
                      <td>
                        @if (isset($category->type_subs))
                          @switch($category->type_subs)
                            @case(1)
                            Trial
                            @break
                            @case(2)
                            Beginner
                            @break
                            @case(3)
                            Intermediate
                            @break
                            @case(4)
                            Advance
                            @break
                          @endswitch
                        @else
                          Belum ditentukan
                        @endif
                      </td>
                      <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                        data-target="#aksi-{{ $category->id }}">UBAH
                      </button></td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @foreach($categories as $category)
    <div class="modal fade" id="aksi-{{ $category->id }}" style="display: none;">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="{{ route('kategori.update', $category->id) }}" method="POST">
            @method('PATCH')
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"
              aria-label="Close">
              <span aria-hidden="true">×</span></button>
              <h4 class="modal-title">Edit Kategori: {{ $category->name }}</h4>
            </div>
            <div class="modal-body">
              @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="kategori">Nama Kategori</label>
                  <input type="text" class="form-control" id="kategori" placeholder="Masukkan nama kategori" name="name" autocomplete="off" style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII='); cursor: auto;" value="{{ $category->name }}" required>
                </div>
                <div class="form-group">
                  <label for="level-{{ $category->id }}">Materi yang harus diselesaikan terlebih dahulu</label>
                  <select name="level" id="level-{{ $category->id }}" class="form-control" style="width: 100%">
                    <option value="">Tidak ditentukan</option>
                    @foreach($categories as $kategori)
                      <option value="{{ $kategori->id }}" {{ $kategori->id == $category->level ? 'selected' : '' }}>{{ $kategori->name }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label for="type_subs-{{ $category->id }}">Paket Langganan</label>
                  <select name="type_subs" id="type_subs-{{ $category->id }}" class="form-control" style="width: 100%">
                    <option value="">Tidak ditentukan</option>
                    <option value="1" {{ $category->type_subs == 1 ? 'selected' : ''}}>Trial</option>
                    <option value="2" {{ $category->type_subs == 2 ? 'selected' : ''}}>Beginner</option>
                    <option value="3" {{ $category->type_subs == 3 ? 'selected' : ''}}>Intermediate</option>
                    <option value="4" {{ $category->type_subs == 4 ? 'selected' : ''}}>Advance</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default"
              data-dismiss="modal">Batal
            </button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endforeach
@endsection

@section('js')
  <script>
  $(function () {
    $("#kategori").DataTable();
    @foreach($categories as $category)
    $("#level-{{ $category->id }}").select2();
    @endforeach
  });
  </script>
@stop
