@extends('adminlte::page')

@section('title', 'User')

@section('content_header')
    <h1>Daftar User</h1>
    @if (($message = Session::get('success')) || ($message = Session::get('error')))
        <div class="alert alert-{{ Session::get('success') ? 'success' : 'danger'}} alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! $message !!}
        </div>
    @endif
@endsection

@section('content')
    @php($start = 0)
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="user" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 1%">No.</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Lembaga</th>
                                    <th style="width: 10%">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ ++$start }}.</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                            @if(isset($user->lembaga))
                                                {{ $user->rel_lembaga->nama }}
                                            @endif
                                        </td>
                                        <td><a href="{{ route('user-resetpass', $user->id) }}" class="btn btn-sm btn-danger" onclick="return confirm('Apakah anda yakin untuk melakukan reset password dari {{ $user->name }}')">RESET PASS</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function () {
            $("#user").DataTable();
        });
    </script>
@stop
