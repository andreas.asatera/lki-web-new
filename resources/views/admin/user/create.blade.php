@extends('adminlte::page')

@section('title', 'Tambah User')

@section('content_header')
    <h1>Tambah User</h1>
    @if (($message = Session::get('success')) || ($message = Session::get('error')))
        <div class="alert alert-{{ Session::get('success') ? 'success' : 'danger'}} alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! $message !!}
        </div>
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <button type="button" class="btn btn-block btn-primary btn-lg" data-toggle="modal"
                                        data-target="#modal-bulk">Bulk
                                </button>
                                <div class="modal fade" id="modal-bulk" style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <form action="{{ route('user-bulk') }}" method="POST"
                                                  enctype="multipart/form-data">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span></button>
                                                    <h4 class="modal-title">Add Bulk User</h4>
                                                </div>
                                                <div class="modal-body">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="">Import Data</label>
                                                        <input type="file" accept=".csv" name="file"
                                                               class="{{ $errors->has('file') ? 'is-invalid':'' }}"
                                                               required>
                                                        <p class="text-danger">{{ $errors->first('file') }}</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="lembaga">Lembaga</label><select name="lembaga" id="lembaga" class="form-control lembaga" style="width: 100%" required>
                                                            @foreach($lembaga_arr as $lembaga)
                                                                <option
                                                                    value="{{ $lembaga->kode }}">{{ $lembaga->nama }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="dateend">Akhir Berlangganan (opsional)</label>
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right"
                                                                   id="dateend" name="date_end"
                                                                   placeholder="-" autocomplete="off">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="{{ route('user-bulk-template') }}"
                                                       class="btn btn-success pull-left">DOWNLOAD TEMPLATE</a>
                                                    <button type="button" class="btn btn-default"
                                                            data-dismiss="modal">Batal
                                                    </button>
                                                    <button type="submit" class="btn btn-primary">Tambah User</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <button type="button" class="btn btn-block btn-warning btn-lg" data-toggle="modal"
                                        data-target="#modal-single">Single
                                </button>
                                <div class="modal fade" id="modal-single" style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <form action="{{ route('user-single') }}" method="POST">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span></button>
                                                    <h4 class="modal-title">Add Single User</h4>
                                                </div>
                                                <div class="modal-body">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="nomor_induk">NISN</label>
                                                        <input type="text" class="form-control" id="nomor_induk"
                                                               placeholder="Masukkan nomor induk" name="nomor_induk"
                                                               autocomplete="off"
                                                               style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII='); cursor: auto;">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="lembaga">Lembaga *</label><select name="lembaga" id="lembaga" class="form-control lembaga" style="width: 100%" required>
                                                            @foreach($lembaga_arr as $lembaga)
                                                                <option
                                                                    value="{{ $lembaga->kode }}">{{ $lembaga->nama }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="name">Nama *</label>
                                                        <input type="text" class="form-control" id="name"
                                                               placeholder="Masukkan nama" name="name"
                                                               autocomplete="off"
                                                               style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII='); cursor: auto;"
                                                               required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Email *</label>
                                                        <input type="email" class="form-control" id="email"
                                                               placeholder="Masukkan email" name="email"
                                                               autocomplete="off"
                                                               style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII='); cursor: auto;"
                                                               required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="gender">Gender</label>
                                                        <select name="gender" id="gender" class="form-control">
                                                            <option value="1">Laki-laki</option>
                                                            <option value="2">Perempuan</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="datepicker">Tgl Lahir</label>
                                                        <div class="form-group">
                                                            <div class="input-group date">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                                <input type="text" class="form-control pull-right"
                                                                       id="datepicker" name="tgl_lahir">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="alamat">Alamat</label>
                                                        <textarea name="alamat" id="alamat" class="form-control"
                                                                  placeholder="Masukkan alamat"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="no_hp">No HP *</label>
                                                        <input type="text" class="form-control" id="no_hp"
                                                               placeholder="Masukkan no hp" name="no_hp"
                                                               autocomplete="off"
                                                               style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII='); cursor: auto;"
                                                               required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="password">Password*</label>
                                                        <input type="password" class="form-control" id="password"
                                                               placeholder="Masukkan password" name="password"
                                                               autocomplete="off"
                                                               style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII='); cursor: auto;"
                                                               required>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default pull-left"
                                                            data-dismiss="modal">Batal
                                                    </button>
                                                    <button type="submit" class="btn btn-primary">Tambah User</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function () {
            $("#id_sekolah").select2();
            $("#id_sekolah_bulk").select2();
            $(".lembaga").select2();
            $('#datepicker').datepicker({
                autoclose: true
            });
            $('#dateend').datepicker({
                autoclose: true,
                format: '{{ config('app.date_format_javascript') }}',
                locale: 'en',
                startDate: new Date()
            }).on("changeDate", function (e) {
                $('#dateend').attr('value', e.format());
            });
        });
    </script>
@stop
