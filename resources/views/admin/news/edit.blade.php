@extends('adminlte::page')

@section('title', 'Berita')

@section('content_header')
    <h1>Edit Berita</h1>
    @if (($message = Session::get('success')) || ($message = Session::get('error')))
        <div class="alert alert-{{ Session::get('success') ? 'success' : 'danger'}} alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! $message !!}
        </div>
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Berita</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{ route('news.update', $news->id) }}" method="post" enctype="multipart/form-data">
                    @method('put')
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="judul">Judul Berita</label>
                            <input type="text" class="form-control" id="judul" placeholder="Masukkan judul berita"
                                   name="judul" required autocomplete="off" value="{{ $news->judul }}"
                                   style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII='); cursor: auto;">
                        </div>
                        <div class="form-group">
                            <label for="isi">Isi Berita</label>
                            @trix($news, 'isi')
                        </div>
                        <div class="form-group">
                            <img src="{{ asset("storage")."/$news->image" }}" class="img-fluid" alt=""><br>
                            <label for="image">Banner Berita (opsional)</label>
                            <input type="file" name="image" id="image">
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">SIMPAN</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('css')
    @trixassets
@endsection