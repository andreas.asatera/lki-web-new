@extends('adminlte::page')

@section('title', 'Berita')

@section('content_header')
    <h1>Daftar Berita</h1>
    @if (($message = Session::get('success')) || ($message = Session::get('error')))
        <div class="alert alert-{{ Session::get('success') ? 'success' : 'danger'}} alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! $message !!}
        </div>
    @endif
@endsection

@section('content')
    @php($start = 0)
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="news" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 1%">No.</th>
                                    <th>Judul Pemberitahuan</th>
                                    <th>Tanggal Diumumkan</th>
                                    <th>Tanggal Update</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($newscollection as $news)
                                    <tr>
                                        <td>{{ ++$start }}.</td>
                                        <td>{{ $news->judul }}</td>
                                        <td>{{ $news->created_at }}</td>
                                        <td>{{ $news->updated_at }}</td>
                                        <td>
                                            <a href="{{ route('news.edit', $news->id) }}"><button class="btn btn-primary btn-sm">EDIT</button></a>
                                            <button type="submit" class="btn btn-danger btn-sm mr-2" form="del-subs-{{ $news->id }}">DELETE</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @foreach($newscollection as $news)
    <form action="{{ route('news.destroy', $news->id) }}" class="delete" method="post" id="del-subs-{{ $news->id }}">
        @csrf
        @method('DELETE')
    </form>
    @endforeach
@endsection

@section('js')
    <script>
        $(function () {
            $("#news").DataTable();
            $(".delete").on("submit", function(){
                return confirm("Are you sure?");
            });
        });
    </script>
@stop