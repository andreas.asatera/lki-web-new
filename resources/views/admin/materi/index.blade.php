@extends('adminlte::page')

@section('title', 'Materi')

@section('content_header')
    <h1>Daftar Materi</h1>
@endsection

@section('content')
    @php($start = 0)
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="materi" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 1%">No.</th>
                                    <th>Nama Materi</th>
                                    <th>Kategori</th>
                                    <th style="width: 10%">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($materies as $materi)
                                    <tr>
                                        <td>{{ ++$start }}.</td>
                                        <td>{{ $materi->judul }}</td>
                                        <td>{{ $materi->category->name }}</td>
                                        <td><a href="{{ route('materi.edit', $materi->id) }}" class="btn btn-primary btn-sm">EDIT</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function () {
            $("#materi").DataTable();
        });
    </script>
@stop
