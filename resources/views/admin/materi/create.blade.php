@extends('adminlte::page')

@section('title', 'Tambah Materi')

@section('content_header')
    <h1>Tambah Materi</h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tambah Materi</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{ route('materi.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                {!! $message !!}
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="judul">Judul Materi</label>
                            <input type="text" class="form-control" id="judul" placeholder="Masukkan judul materi"
                                   name="judul" autocomplete="off"
                                   style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII='); cursor: auto;">
                        </div>
                        <div class="form-group">
                            <label for="ringkasan">Ringkasan Materi</label>
                            <textarea name="ringkasan" id="ringkasan" class="form-control"
                                      placeholder="Masukkan ringkasan materi"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="materi">File Materi</label>
                            <input type="file" name="materi" id="materi">
                        </div>
                        <div class="form-group">
                            <label for="video">Video Materi</label>
                            <input type="file" name="video" id="video">
                        </div>
                            <div class="form-group">
                                <label for="kategori">Kategori Materi</label>
                                <select name="kategori" id="kategori" class="form-control">
                                    @foreach($kategories as $kategori)
                                        <option value="{{ $kategori->id }}">{{ $kategori->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">TAMBAH</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function () {
            $("#kategori").select2();
        });
    </script>
@stop