@extends('adminlte::page')

@section('title', 'Daftar Soal')

@section('content_header')
    <h1>Daftar Soal</h1>
@endsection

@section('content')
    @php($start = 0)
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="soal" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 1%">No.</th>
                                    <th>Soal</th>
                                    <th>Gambar</th>
                                    <th>Opsi 1</th>
                                    <th>Opsi 2</th>
                                    <th>Opsi 3</th>
                                    <th>Opsi 4</th>
                                    <th>Jawaban</th>
                                    <th>Kategori</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($soals as $soal)
                                    <tr>
                                        <td>{{ ++$start }}.</td>
                                        <td>{{ $soal->question }}</td>
                                        <td>{{ $soal->gambar }}</td>
                                        <td>{{ $soal->option1 }}</td>
                                        <td>{{ $soal->option2 }}</td>
                                        <td>{{ $soal->option3 }}</td>
                                        <td>{{ $soal->option4 }}</td>
                                        <td>{{ $soal->answerNr }}</td>
                                        <td>{{ $soal->categoryID }}</td>
                                        <td><a href="{{ route('soal.edit', $soal->id) }}" class="btn btn-sm btn-primary">UBAH</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function () {
            $("#soal").DataTable();
        });
    </script>
@stop