@extends('adminlte::page')

@section('title', 'Soal')

@section('content_header')

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Soal</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{ route('soal.update', $soal->id) }}" method="post" enctype="multipart/form-data">
                    @method('put')
                    @csrf
                    <div class="box-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                {!! $message !!}
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="question">Soal</label>
                            <textarea name="question" id="question" class="form-control"
                                      placeholder="Masukkan soal">{{ $soal->question }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="image">Gambar Soal</label>
                            <input type="file" name="image" id="image">
                        </div>
                        <div class="form-group">
                            <label for="option1">Opsi A</label>
                            <input type="text" class="form-control" id="option1" placeholder="Masukkan opsi jawaban"
                                   name="option1" autocomplete="off" value="{{ $soal->option1 }}">
                        </div>
                        <div class="form-group">
                            <label for="option2">Opsi B</label>
                            <input type="text" class="form-control" id="option2" placeholder="Masukkan opsi jawaban"
                                   name="option2" autocomplete="off" value="{{ $soal->option2 }}">
                        </div>
                        <div class="form-group">
                            <label for="option3">Opsi C</label>
                            <input type="text" class="form-control" id="option3" placeholder="Masukkan opsi jawaban"
                                   name="option3" autocomplete="off"value="{{ $soal->option3 }}">
                        </div>
                        <div class="form-group">
                            <label for="option4">Opsi D</label>
                            <input type="text" class="form-control" id="option4" placeholder="Masukkan opsi jawaban"
                                   name="option4" autocomplete="off"value="{{ $soal->option4 }}">
                        </div>
                        <div class="form-group">
                            <label for="answerNr">Jawaban</label>
                            <select name="answerNr" id="answerNr" class="form-control">
                                <option value="1" {{ $soal->answerNr == 1 ? 'selected' : '' }}>A</option>
                                <option value="2" {{ $soal->answerNr == 2 ? 'selected' : '' }}>B</option>
                                <option value="3" {{ $soal->answerNr == 3 ? 'selected' : '' }}>C</option>
                                <option value="4" {{ $soal->answerNr == 4 ? 'selected' : '' }}>D</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="categoryID">Kategori</label>
                            <select name="categoryID" id="categoryID" class="form-control">
                                @foreach($kategories as $kategori)
                                    <option value="{{ $kategori->id }}" {{ $kategori->id == $soal->categoryID ? 'selected' : '' }}>{{ $kategori->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">SIMPAN</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function () {
            $("#categoryID").select2();
        });
    </script>
@stop