@extends('adminlte::page')

@section('title', 'Tambah Soal')

@section('content_header')

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tambah Soal</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{ route('soal.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                {!! $message !!}
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="question">Soal</label>
                            <textarea name="question" id="question" class="form-control"
                                      placeholder="Masukkan soal"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="image">Gambar Soal</label>
                            <input type="file" name="image" id="image">
                        </div>
                        <div class="form-group">
                            <label for="option1">Opsi A</label>
                            <input type="text" class="form-control" id="option1" placeholder="Masukkan opsi jawaban"
                                   name="option1" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label for="option2">Opsi B</label>
                            <input type="text" class="form-control" id="option2" placeholder="Masukkan opsi jawaban"
                                   name="option2" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label for="option3">Opsi C</label>
                            <input type="text" class="form-control" id="option3" placeholder="Masukkan opsi jawaban"
                                   name="option3" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label for="option4">Opsi D</label>
                            <input type="text" class="form-control" id="option4" placeholder="Masukkan opsi jawaban"
                                   name="option4" autocomplete="off">
                        </div>
                            <div class="form-group">
                                <label for="answerNr">Jawaban</label>
                                <select name="answerNr" id="answerNr" class="form-control">
                                    <option value="1">A</option>
                                    <option value="2">B</option>
                                    <option value="3">C</option>
                                    <option value="4">D</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="categoryID">Kategori</label>
                                <select name="categoryID" id="categoryID" class="form-control">
                                    @foreach($kategories as $kategori)
                                        <option value="{{ $kategori->id }}">{{ $kategori->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">TAMBAH</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function () {
            $("#categoryID").select2();
        });
    </script>
@stop