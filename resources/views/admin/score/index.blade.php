@extends('adminlte::page')

@section('title', 'Laporan Nilai')

@section('content_header')
    <h1>Laporan Nilai</h1>
    @if (($message = Session::get('success')) || ($message = Session::get('error')))
        <div class="alert alert-{{ Session::get('success') ? 'success' : 'danger'}} alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! $message !!}
        </div>
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('nilai.store') }}" method="POST">
                                @csrf
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="lembaga">Nama Lembaga</label>
                                        <select name="lembaga" id="lembaga" class="form-control"
                                                style="display: none; width: 100%" required>
                                            <option></option>
                                            @foreach($lembaga_arr as $lembaga)
                                                <option value="{{ $lembaga->kode }}">{{ $lembaga->kode }}
                                                    - {{ $lembaga->nama }}</option>
                                            @endforeach
                                        </select>
                                        <label for="cid">Nama Materi</label>
                                        <select name="cid" id="cid" class="form-control select2"
                                                style="display: none; width: 100%" required>
                                            <option></option>
                                            @foreach($materi_arr as $materi)
                                                <option value="{{ $materi->id }}">{{ $materi->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="col-sm-12">
                                            <div class="box-footer text-center">
                                                <button type="submit" class="btn btn-info">Lihat Laporan</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select2-bootstrap.min.css') }}">
@endsection

@section('js')
    <!-- DataTables -->
    <script src="{{ asset('js/select2.js') }}"></script>
    <script type="text/javascript">
        $('#lembaga').select2({
            theme: 'bootstrap',
            placeholder: 'PILIH LEMBAGA',
            dropdownAutoWidth: true
        })
        $('#cid').select2({
            theme: 'bootstrap',
            placeholder: 'PILIH MATERI',
            dropdownAutoWidth: true
        })
    </script>
@endsection
