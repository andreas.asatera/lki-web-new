@extends('adminlte::page')

@section('title', 'Laporan Nilai')

@section('content_header')
    <div class='page-header' style="margin: 0; padding: 0;">
        <div class='btn-toolbar pull-right'>
            <div class='btn-group'>
                <button type='button' class='btn btn-danger' data-toggle="modal"
                        data-target='#hapus'>Hapus Laporan
                </button>
            </div>
        </div>
        <h1>Laporan Nilai</h1>
    </div>
@endsection

@section('content')
    @php($start = 0)
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="lembagas" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 1%">No.</th>
                                    <th>Nama</th>
                                    <th>Nilai 1</th>
                                    <th>Nilai 2</th>
                                    <th>Nilai 3</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ ++$start }}.</td>
                                        <td>{{$user->name}}</td>
                                        <td>
                                            @if(isset($user->score[0]))
                                                {{ $user->score[0]->score }}
                                            @else
                                                Belum mengerjakan tes
                                            @endif
                                        </td>
                                        <td>
                                            @if(isset($user->score[1]))
                                                {{ $user->score[1]->score }}
                                            @else
                                                Belum mengerjakan tes
                                            @endif
                                        </td>
                                        <td>
                                            @if(isset($user->score[2]))
                                                {{ $user->score[2]->score }}
                                            @else
                                                Belum mengerjakan tes
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="hapus" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('delete-score-cid') }}" method="POST">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Hapus Laporan</h4>
                    </div>
                    <div class="modal-body">
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label for="lembaga">Nama Lembaga</label>
                                <input type="text" class="form-control" id="lembaga" autocomplete="off"
                                       style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII='); cursor: auto;"
                                       value="{{ $lembaga->nama }} ({{ $lembaga->kode }})" required readonly>
                            </div>
                            <div class="form-group">
                                <label for="kategori">Nama Kategori</label>
                                <input type="text" class="form-control" id="kategori" autocomplete="off"
                                       style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII='); cursor: auto;"
                                       value="{{ $kategori->name }} ({{ $kategori->id }})" required readonly>
                            </div>
                            <input type="text" name="lembaga" value="{{ $lembaga->kode }}" hidden>
                            <input type="text" name="cid" value="{{ $kategori->id }}" hidden>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">BATAL
                        </button>
                        <button type="submit" class="btn btn-danger">HAPUS</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.js"></script>
    <script>
        $(function () {
            /*$('#usersList').DataTable( {
                buttons: [
                    'copy', 'excel', 'pdf'
                ]
            })*/
            var table = $('#lembagas').DataTable({
                buttons: [
                    {
                        extend: 'copy',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                    },
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                    },
                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                    }
                ]
            });

            table.buttons().container()
                .appendTo($('.col-sm-6:eq(0)', table.table().container()));
        })
    </script>
@endsection
