@extends('adminlte::page')

@section('title', 'Subscription')

@section('content_header')
  <h1>User Subscription</h1>
  @if (($message = Session::get('success')) || ($message = Session::get('error')))
    <div class="alert alert-{{ Session::get('success') ? 'success' : 'danger'}} alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      {!! $message !!}
    </div>
  @endif
@endsection

@section('content')
  @php($start = 0)
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <table id="user" class="table table-bordered table-striped table-responsive" style="width: 100%">
                <thead>
                  <tr>
                    <th style="width: 1%">No.</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>No. HP</th>
                    <th>Lembaga</th>
                    <th>Awal Berlangganan</th>
                    <th>Akhir Berlangganan</th>
                    <th>Tipe Langganan</th>
                    <th style="width: 10%">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($users as $user)
                    <tr>
                      <td>{{ ++$start }}.</td>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->email }}</td>
                      <td>{{ isset($user->no_hp) ? $user->no_hp : '-' }}</td>
                      <td>
                        @if(isset($user->lembaga))
                            {{ $user->rel_lembaga->nama }}
                        @endif
                      </td>
                      <td>{{ isset($user->subscription) && !is_null($user->subscription->start_date) ? $user->subscription->start_date : "-" }}</td>
                      <td>{{ isset($user->subscription) && !is_null($user->subscription->start_date) ? $user->subscription->end_date : "-" }}</td>
                      <td>
                        @if(isset($user->subscription->type_subs))
                          @switch($user->subscription->type_subs)
                            @case(1)
                            Trial
                            @break
                            @case(2)
                            Beginner
                            @break
                            @case(3)
                            Intermediate
                            @break
                            @case(4)
                            Advance
                            @break
                          @endswitch
                        @else
                          Belum ditentukan
                        @endif
                      </td>
                      <td align="center">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                        data-target="#aksi-{{ $user->id }}">AKSI
                      </button>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@foreach($users as $user)
  <div class="modal fade" id="aksi-{{ $user->id }}" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{ route('subscription.update', $user->id) }}" method="POST">
          @method('PATCH')
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
            aria-label="Close">
            <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Subscription: {{ $user->name }}</h4>
          </div>
          <div class="modal-body">
            @csrf
            <div class="form-group">
              <label for="type_subs">Type Langganan</label>
              <select id="type_subs" name="type_subs" class="form-control" required>
                <option value="1" {{ isset($user->subscription->type_subs) && $user->subscription->type_subs == 1 ? "selected" : "" }}>Trial - 15 Materi</option>
                <option value="2" {{ isset($user->subscription->type_subs) && $user->subscription->type_subs == 2 ? "selected" : "" }}>Beginner - 45 Materi</option>
                <option value="3" {{ isset($user->subscription->type_subs) && $user->subscription->type_subs == 3 ? "selected" : "" }}>Intermediate - 63 Materi</option>
                <option value="4" {{ isset($user->subscription->type_subs) && $user->subscription->type_subs == 4 ? "selected" : "" }}>Advance - Semua Materi</option>
              </select>
            </div>
            <div class="form-group">
              <label for="datestart">Awal Berlangganan</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right"
                id="datestart" name="start_date" readonly
                value="{{ isset($user->subscription) ? $user->subscription->start_date : "" }}"
                placeholder="-">
              </div>
            </div>
            <div class="form-group">
              <label for="dateend-{{ $user->id }}">Akhir Berlangganan</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right"
                id="dateend-{{ $user->id }}" name="end_date"
                value="{{ isset($user->subscription) ? $user->subscription->end_date : "" }}"
                placeholder="-" autocomplete="off" required>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            @if(isset($user->subscription))
              <button type="submit" class="btn btn-danger pull-left" form="del-subs-{{ $user->id }}">
                Nonaktif Berlangganan
              </button>
            @endif
            <button type="button" class="btn btn-default"
            data-dismiss="modal">Batal
          </button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
      <form action="{{ route('subscription.destroy', $user->id) }}" method="post"
        id="del-subs-{{ $user->id }}">
        @csrf
        @method('DELETE')
      </form>
    </div>
  </div>
</div>
@endforeach
@endsection

@section('js')
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
  <script type="text/javascript"
          src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.js"></script>
  <script>
  $(document).ready(function () {
    var table = $("#user").DataTable({
      buttons: [
        {
          extend: 'copy',
          exportOptions: {
            columns: 'th:not(:last-child)'
          }
        },
        {
          extend: 'excel',
          exportOptions: {
            columns: 'th:not(:last-child)'
          }
        },
        {
          extend: 'pdf',
          exportOptions: {
            columns: 'th:not(:last-child)'
          }
        }
      ]
    });
    table.buttons().container()
    .appendTo($('.col-sm-6:eq(0)', table.table().container()));
    @foreach($users as $user)
    $('#dateend-{{ $user->id }}').datepicker({
      autoclose: true,
      format: '{{ config('app.date_format_javascript') }}',
      locale: 'en',
      startDate: new Date()
    }).on("changeDate", function (e) {
      $('#dateend-{{ $user->id }}').attr('value', e.format());
    });
    @endforeach
  });
  </script>
@endsection
