@extends('adminlte::page')

@section('title', 'Tambah Subscription')

@section('content_header')
    <h1>Tambah Subscription</h1>
    @if (($message = Session::get('success')) || ($message = Session::get('error')))
        <div class="alert alert-{{ Session::get('success') ? 'success' : 'danger'}} alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! $message !!}
        </div>
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <button type="button" class="btn btn-block btn-primary btn-lg" data-toggle="modal"
                                        data-target="#modal-bulk">Bulk
                                </button>
                                <div class="modal fade" id="modal-bulk" style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <form action="{{ route('subscription-bulk') }}" method="POST"
                                                  enctype="multipart/form-data">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span></button>
                                                    <h4 class="modal-title">Add Bulk Subscription</h4>
                                                </div>
                                                <div class="modal-body">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="">Import Data</label>
                                                        <input type="file" accept=".csv" name="file"
                                                               class="{{ $errors->has('file') ? 'is-invalid':'' }}"
                                                               required>
                                                        <p class="text-danger">{{ $errors->first('file') }}</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="start_date">Awal Berlangganan</label>
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right"
                                                                   id="start_date" name="start_date"
                                                                   placeholder="-" autocomplete="off" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="end_date">Akhir Berlangganan</label>
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right"
                                                                   id="end_date" name="end_date"
                                                                   placeholder="-" autocomplete="off" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="type_subs">Paket Langganan</label><select name="type_subs" id="type_subs" class="form-control lembaga" style="width: 100%" required>
                                                            <option value="1">Trial</option>
                                                            <option value="2">Beginner</option>
                                                            <option value="3">Intermediate</option>
                                                            <option value="4">Advance</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default"
                                                            data-dismiss="modal">Batal
                                                    </button>
                                                    <button type="submit" class="btn btn-primary">Tambah Subscription</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function () {
            $("#type_subs").select2();
            $('#start_date').datepicker({
                autoclose: true,
                format: '{{ config('app.date_format_javascript') }}',
                locale: 'en'
            }).on("changeDate", function (e) {
                $('#start_date').attr('value', e.format());
            });
            $('#end_date').datepicker({
                autoclose: true,
                format: '{{ config('app.date_format_javascript') }}',
                locale: 'en'
            }).on("changeDate", function (e) {
                $('#end_date').attr('value', e.format());
            });
        });
    </script>
@endsection

