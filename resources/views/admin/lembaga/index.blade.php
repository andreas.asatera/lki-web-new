@extends('adminlte::page')

@section('title', 'Lembaga')

@section('content_header')
        <div class='page-header' style="margin: 0; padding: 0;">
            <div class='btn-toolbar pull-right'>
                <div class='btn-group'>
                    <button type='button' class='btn btn-primary' data-toggle="modal"
                            data-target='#tambah'>Tambah Lembaga</button>
                </div>
            </div>
            <h1>Lembaga</h1>
        </div>

    @if (($message = Session::get('success')) || ($message = Session::get('error')))
        <div class="alert alert-{{ Session::get('success') ? 'success' : 'danger'}} alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! $message !!}
        </div>
    @endif
@endsection

@section('content')
    @php($start = 0)
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="lembaga" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 1%">No.</th>
                                    <th>Nama Lembaga</th>
                                    <th>Kode Lembaga</th>
                                    <th style="width: 10%">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($lembaga_arr as $lembaga)
                                    <tr>
                                        <td>{{ ++$start }}.</td>
                                        <td>{{ $lembaga->nama }}</td>
                                        <td>{{ $lembaga->kode }}</td>
                                        <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                                    data-target="#aksi-{{ $lembaga->id }}">UBAH
                                            </button></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @foreach($lembaga_arr as $lembaga)
        <div class="modal fade" id="aksi-{{ $lembaga->id }}" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="{{ route('lembaga.update', $lembaga->id) }}" method="POST">
                        @method('PATCH')
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Edit Lembaga: {{ $lembaga->nama }}</h4>
                        </div>
                        <div class="modal-body">
                            @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="kategori">Nama Kategori</label>
                                    <input type="text" class="form-control" id="kategori" placeholder="Masukkan nama kategori" name="nama" autocomplete="off" style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII='); cursor: auto;" value="{{ $lembaga->nama }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="{{ route('lembaga.destroy', $lembaga->id) }}" class="btn btn-danger pull-left">Hapus Lembaga</a>
                            <button type="button" class="btn btn-default"
                                    data-dismiss="modal">Batal
                            </button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
    <div class="modal fade" id="tambah" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('lembaga.store') }}" method="POST">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Tambah Lembaga</h4>
                    </div>
                    <div class="modal-body">
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label for="nama">Nama Lembaga</label>
                                <input type="text" class="form-control" id="nama" placeholder="Masukkan nama lembaga" name="nama" autocomplete="off" style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII='); cursor: auto;" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">Batal
                        </button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function () {
            $("#lembaga").DataTable();
            @foreach($lembaga_arr as $lembaga)
            $("#lembaga-{{ $lembaga->id }}").select2();
            @endforeach
        });
    </script>
@stop
