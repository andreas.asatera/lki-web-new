@extends('layouts.news')

@section('title', 'News')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>
                        Laboratorium Kampung Inggris
                        <small>News</small>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($news_collection as $news)
                <div class="col-md-12">
                    <div class="row">
                        @if(isset($news->image))
                        <div class="col-md-1 text-center">
                            <img class="img-thumbnail" alt=""
                                 src="{{ asset('storage/')."/$news->image" }}">
                        </div>
                        @endif
                        <div class="col-md-{{ isset($news->image) ? '11' : 12 }}">
                            <div class="text-center">
                                <h1>
                                    {{ $news->judul }}
                                </h1><br>
                                <i class="glyphicon glyphicon-calendar"></i> {{ $news->created_at }}<br>
                                <a href="{{ route('api.news.show', $news->id) }}">View
                                    details »</a>
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
            @endforeach
        </div>
    </div>
@endsection

