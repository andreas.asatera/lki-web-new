@extends('layouts.news')

@section('title', 'News')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header text-center">
                    <h1>
                        {{ $news->judul }}<br>
                    </h1>
                    <i class="glyphicon glyphicon-calendar"></i> {{ $news->created_at }}<br>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="text-center col-md-12">
                <img class="img-responsive" alt=""
                     src="{{ asset('storage/')."/$news->image" }}">
            </div>
            <div class="col-md-12">
                {!! $richtexts !!}
            </div>
        </div>
    </div>
@endsection

