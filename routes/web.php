<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::post('admin/login', 'Admin\AdminController@login');
Route::group(['middleware' => ['auth:admin,admin_tutor']], function(){
    Route::post('admin/logout', 'Admin\AdminController@logout');
    Route::get('materi/get/{type}/{name}/{ext}', 'Admin\MateriController@get');
    Route::get('admin/materi/generate-thumbnail', 'Admin\MateriController@generateThumbs');
    Route::resource('admin/materi', 'Admin\MateriController');
    Route::resource('admin/kategori', 'Admin\KategoriController');
    Route::resource('admin/sekolah', 'Admin\SekolahController');
    Route::resource('admin/guru', 'Admin\GuruController');
    Route::resource('admin/soal', 'Admin\SoalController');
    Route::get('admin/user/resetpass/{id}', 'Admin\UserController@resetpass')->name('user-resetpass');
    Route::post('admin/user/add/single', 'Admin\UserController@single')->name('user-single');
    Route::post('admin/user/add/bulk', 'Admin\UserController@bulk')->name('user-bulk');
    Route::get('admin/user/add/bulk', function (){
        return response()->download(storage_path('app/public/template.xls'));
    })->name('user-bulk-template');
    Route::resource('admin/user', 'Admin\UserController');
    Route::resource('admin/tutor', 'Admin\TutorController');
    Route::resource('admin/payment', 'Admin\PaymentController');
    Route::post('admin/subscription/add/bulk', 'Admin\SubscriptionController@bulk')->name('subscription-bulk');
    Route::resource('admin/subscription', 'Admin\SubscriptionController');
    Route::resource('admin/news', 'Admin\NewsController');
    Route::resource('admin/settings', 'Admin\ServerSettingsController');
    Route::resource('admin/lembaga', 'Admin\LembagaController');
    Route::post('admin/nilai/delcid', 'Admin\ScoreController@deleteScoreCID')->name('delete-score-cid');
    Route::post('admin/nilai/massscoredelete', 'Admin\ScoreController@massScoreDelete')->name('mass-score-delete');
    Route::get('admin/nilai/hapuslaporan', 'Admin\ScoreController@massScoreDeleteView');
    Route::resource('admin/nilai', 'Admin\ScoreController');
    Route::resource('admin', 'Admin\AdminController');
});
