<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::group(['middleware' => 'auth:api'], function () {
    Route::group(['prefix' => 'siswa'], function () {
        Route::get('category/materi', 'API\CategoryController@index_materi');
        Route::get('category/soal', 'API\CategoryController@index_soal');
        Route::get('materi/video/{name}', 'API\MateriController@video');
        Route::get('materi/materi/{name}', 'API\MateriController@pdf');
        Route::post('score/check', 'API\ScoreController@check');
        Route::apiResource('subscription', 'API\SubscriptionController');
        Route::post('payment/upload', 'API\PaymentController@upload');
        Route::apiResource('payment', 'API\PaymentController');
        Route::apiResource('score', 'API\ScoreController');
        Route::apiResource('check', 'API\CheckController');
        Route::apiResource('materi', 'API\MateriController');
        Route::apiResource('category', 'API\CategoryController');
        Route::apiResource('soal', 'API\SoalController');
        Route::name('api.')->group(function () {
            Route::apiResource('news', 'API\NewsController');
        });
    });
    Route::apiResource('tutor', 'API\TutorController');
    Route::post('islogin', 'API\UserController@isLoggedIn');
    Route::post('user/upload', 'API\UserController@upload')->name('api-upload-pic');
    Route::apiResource('user', 'API\UserController');
});

Route::group(['middleware' => 'auth:api_admin_tutor'], function () {
    Route::group(['prefix' => 'admin'], function () {
        Route::apiResource('user', 'API\admin\UserController');
    });
});
