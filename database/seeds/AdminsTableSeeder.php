<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('admins')->insert([
            'name' => 'Andreas Asatera',
            'email' => 'andreas.asatera@gmail.com',
            'password' => bcrypt('Andre1995'),
        ]);
    }
}
