<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeLevel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('subscriptions', function (Blueprint $table) {
            //
            $table->integer('type_subs')->after('end_date')->nullable();
        });
        Schema::table('kategoris', function (Blueprint $table) {
            //
            $table->integer('type_subs')->after('name')->nullable();
        });
        Schema::table('payments', function (Blueprint $table) {
            //
            $table->integer('type_subs')->after('user_id')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('subscriptions', function (Blueprint $table) {
            //
            $table->dropColumn('type_subs');
        });
        Schema::table('kategoris', function (Blueprint $table) {
            //
            $table->dropColumn('type_subs');
        });
        Schema::table('payments', function (Blueprint $table) {
            //
            $table->dropColumn('type_subs');
        });
    }
}
