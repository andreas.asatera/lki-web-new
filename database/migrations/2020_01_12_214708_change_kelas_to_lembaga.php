<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeKelasToLembaga extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('kelas', 'lembaga');
        Schema::table('lembaga', function (Blueprint $table) {
            //
            $table->string('kode')->after('nama')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('lembaga', 'kelas');
        Schema::table('lembaga', function (Blueprint $table) {
            //
            $table->dropColumn('kode');
        });
    }
}
